//
//  BaseViewController.h
//  WMPlayer
//
//  Created by 郑文明 on 16/3/15.
//  Copyright © 2016年 郑文明. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "UIViewController+GestureStateBlock.h"
#import "MBProgressHUD.h"
#import "UIViewController+BackButtonHandler.h"
#import "XXEmptyView.h"

@interface BaseViewController : UIViewController
/**
 用了自定义的手势返回，则系统的手势返回屏蔽
 不用自定义的手势返回，则系统的手势返回启用
 */
@property (nonatomic, assign) BOOL enablePanGesture;//是否支持自定义拖动pop手势，默认yes,支持手势

@property (nonatomic,retain) MBProgressHUD* hud;
- (void)addHud;
- (void)addHudWithMessage:(NSString*)message;
- (void)removeHud;








//- (BOOL)navigationShouldPopOnBackButton 可以拦截系统的返回事件


/** 空视图 */
@property (nonatomic, strong) XXEmptyView *emptyView;


/**
 创建UI
 */
- (void)setupViews;


/**
 布局
 */
- (void)setupLayout;


/**
 开始网络请求
 
 @param show 是否显示loading
 */
- (void)requestDataWithShowLoading:(BOOL)show;


/**
 配置空视图，在子控制器调用
 */
- (void)configEmptyView;


/**
 显示空视图
 
 @param errorTitle <#errorTitle description#>
 @param errorMessage <#errorMessage description#>
 */
- (void)showEmpty:(NSString *)errorTitle message:(NSString *)errorMessage;


/* 错误标题 */
@property (nonatomic, copy) NSString *errorTitle;


/* 错误详细 */
@property (nonatomic, copy) NSString *errorMessage;



@end
