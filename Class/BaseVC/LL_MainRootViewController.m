//
//  LL_MainRootViewController.m
//  XJW
//
//  Created by John on 2019/7/27.
//  Copyright © 2019 Liam. All rights reserved.
//

#import "LL_MainRootViewController.h"
#import "LL_MainTabbarViewController.h"
#import "LL_CYLPlusButtonSubclass.h"

@interface LL_MainRootViewController ()

@end

@implementation LL_MainRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBarHidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    [self createNewTabBar];
}

- (CYLTabBarController *)createNewTabBar {
    [LL_CYLPlusButtonSubclass registerPlusButton];
    return [self createNewTabBarWithContext:nil];
}

- (CYLTabBarController *)createNewTabBarWithContext:(NSString *)context {
    LL_MainTabbarViewController *tabBarController = [[LL_MainTabbarViewController alloc] initWithContext:context];
    self.viewControllers = @[tabBarController];
    return tabBarController;
}








- (void)dealloc {
    NSLog(@"🔴类名与方法名：%@（在第%@行），描述：%@", @(__PRETTY_FUNCTION__), @(__LINE__), @"");
}




@end
