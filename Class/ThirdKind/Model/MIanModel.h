//
//  MIanModel.h
//  rainbowOnline
//
//  Created by MacBook on 27/03/2019.
//  Copyright © 2019 MacBook. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MIanModel : NSObject
@property(nonatomic,strong)NSNumber *searchValue;
@property(nonatomic,strong)NSString *createBy;
@property(nonatomic,strong)NSString *createTime;
@property(nonatomic,strong)NSString *updateBy;
@property(nonatomic,strong)NSString *updateTime;
@property(nonatomic,strong)NSString *remark;
@property(nonatomic,strong)NSDictionary *params;
@property(nonatomic,strong)NSNumber *Id;
@property(nonatomic,strong)NSNumber *appId;
@property(nonatomic,strong)NSString *appName;
@property(nonatomic,strong)NSString *addTime;
@property(nonatomic,strong)NSString *shareContent;
@property(nonatomic,strong)NSString *serviceUrl;
@property(nonatomic,strong)NSString *imageUrl;
@property(nonatomic,strong)NSString *updateUrl;
@property(nonatomic,strong)NSString *jumpUrl;
@property(nonatomic,strong)NSNumber *isMenu;
@property(nonatomic,strong)NSString *titleOne;
@property(nonatomic,strong)NSString *titleOneName;
@property(nonatomic,strong)NSString *titleTwo;
@property(nonatomic,strong)NSString *titleTwoName;
@property(nonatomic,strong)NSString *titleThree;
@property(nonatomic,strong)NSString *titleTitleThreeName;
//
@property(nonatomic,strong)NSString *title__One;
@property(nonatomic,strong)NSString *title__Two;
@property(nonatomic,strong)NSString *title__Three;
@property(nonatomic,strong)NSString *title_OneName;
@property(nonatomic,strong)NSString *title_TwoName;
@property(nonatomic,strong)NSString *title_ThreeName;

@end


