//
//  MIanModel.m
//  rainbowOnline
//
//  Created by MacBook on 27/03/2019.
//  Copyright © 2019 MacBook. All rights reserved.
//

#import "MIanModel.h"
#import "YYModel.h"


@implementation MIanModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID":@"id",
             @"title__One":@"newTitleOne",
             @"title__Two":@"newTitleTwo",
             @"title__Three":@"newTitleThree",
             @"title_OneName":@"newTitleOneName",
             @"title_TwoName":@"newTitleTwoName",
             @"title_ThreeName":@"newTitleThreeName"};
}


@end
