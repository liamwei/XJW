//
//  UIButton+ImageLocationButton.h
//  SHAREBYLONNIE
//
//  Created by lonnie on 2017/9/20.
//  Copyright © 2017年 david. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (ImageLocationButton)

typedef NS_ENUM(NSUInteger, MKButtonEdgeInsetsStyle) {
    MKButtonEdgeInsetsStyleTop, // image在上，label在下
    MKButtonEdgeInsetsStyleLeft, // image在左，label在右
    MKButtonEdgeInsetsStyleBottom, // image在下，label在上
    MKButtonEdgeInsetsStyleRight // image在右，label在左
};

- (void)layoutButtonWithEdgeInsetsStyle:(MKButtonEdgeInsetsStyle)style
                        imageTitleSpace:(CGFloat)space;
@end
