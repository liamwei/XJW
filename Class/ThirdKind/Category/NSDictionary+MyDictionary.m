//
//  NSDictionary+MyDictionary.m
//  rainbowOnline
//
//  Created by MacBook on 27/03/2019.
//  Copyright © 2019 MacBook. All rights reserved.
//

#import "NSDictionary+MyDictionary.h"

@implementation NSDictionary (MyDictionary)

- (NSDictionary *)deleteAllNullValue{
    NSMutableDictionary *mutableDic = [[NSMutableDictionary alloc] init];
    for (NSString *keyStr in self.allKeys) {
        if ([[self objectForKey:keyStr] isEqual:[NSNull null]]) {
            [mutableDic setObject:@"" forKey:keyStr];
        }
        else{
            [mutableDic setObject:[self objectForKey:keyStr] forKey:keyStr];
        }
    }
    return mutableDic;
}
@end
