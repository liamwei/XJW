//
//  UIView+Extention.m
//

#import "UIView+Extention.h"

IB_DESIGNABLE
@implementation UIView (Extention)

@dynamic circleCorner;
@dynamic cornerRadius;
@dynamic borderColor;
@dynamic borderWidth;
@dynamic shadowColor;
@dynamic shadowOffset;
@dynamic shadowOpacity;
@dynamic shadowRadius;
@dynamic shadowPath;
@dynamic shadowShouldRasterize;
@dynamic shadowRasterizationScale;
@dynamic maskToBounds;

- (void) setCircleCorner: (BOOL)newValue {
    [self setCornerRadius:self.bounds.size.height/2];
}

- (void) setCornerRadius: (CGFloat)newValue {
    [self.layer setCornerRadius:newValue];
}

- (void) setBorderColor: (UIColor *)newValue {
    [self.layer setBorderColor:newValue.CGColor];
}

- (void) setBorderWidth: (CGFloat)newValue {
    [self.layer setBorderWidth:newValue];
}

- (void) setShadowColor: (UIColor *)newValue {
    [self.layer setShadowColor:newValue.CGColor];
}

- (void) setShadowOffset: (CGSize)newValue {
    [self.layer setShadowOffset:newValue];
}

- (void) setShadowOpacity: (double)newValue {
    [self.layer setShadowOpacity:newValue];
}

- (void) setShadowRadius: (CGFloat)newValue {
    [self.layer setShadowRadius:newValue];
}

- (void) setShadowPath: (CGPathRef)newValue {
    [self.layer setShadowPath:newValue];
}

- (void) setShadowShouldRasterize: (BOOL)newValue {
    [self.layer setShadowRadius:newValue];
}

- (void) setShadowRasterizationScale: (CGFloat)newValue {
    [self.layer setRasterizationScale:newValue];
}

- (void) setMaskToBounds: (BOOL)newValue {
    [self.layer setMasksToBounds:newValue];
}


@end
