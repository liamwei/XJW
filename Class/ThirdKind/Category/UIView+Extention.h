//
//  UIView+Extention.h
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface UIView (Extention)

@property (nonatomic) IBInspectable BOOL circleCorner;
@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable UIColor* borderColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable UIColor* shadowColor;
@property (nonatomic) IBInspectable CGSize shadowOffset;
@property (nonatomic) IBInspectable double shadowOpacity;
@property (nonatomic) IBInspectable CGFloat shadowRadius;
@property (nonatomic) IBInspectable CGPathRef shadowPath;
@property (nonatomic) IBInspectable BOOL shadowShouldRasterize;
@property (nonatomic) IBInspectable CGFloat shadowRasterizationScale;
@property (nonatomic) IBInspectable BOOL maskToBounds;

@end
