//
//  NSDictionary+Log.h
//  rainbowOnline
//
//  Created by MacBook on 30/03/2019.
//  Copyright © 2019 MacBook. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (Log)
- (NSString *)descriptionWithLocale:(id)locale;
@end

NS_ASSUME_NONNULL_END
