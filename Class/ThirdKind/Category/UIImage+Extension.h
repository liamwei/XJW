//
//  UIImage+Extension.h
//

#import <UIKit/UIKit.h>

@interface UIImage (Extension)

/**
 *  根据图片名返回一张能够自由拉伸的图片
 */
+ (UIImage *)resizedImage:(NSString *)name;

/**
 *  根据图片名返回一张不渲染的图片
 */
+ (UIImage *)originalImage:(NSString *)name;

+ (UIImage *)webImgeWithUrl:(NSString *)urlString;

//压缩图片
- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize;

//压缩指定大小图片
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

@end
