//
//  UIColor+Extension.h
//  fmg100
//
//  Created by 李晓 on 16/6/27.
//  Copyright © 2016年 深圳来创智能科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extension)
+ (UIColor*) colorWithHexString:(NSString *)color alpha:(CGFloat)alpha;
+ (UIColor *) colorWithHexString: (NSString *)color;
+ (NSString *) hexFromUIColor: (UIColor*) color;
@end
