//
//  NSDictionary+MyDictionary.h
//  rainbowOnline
//
//  Created by MacBook on 27/03/2019.
//  Copyright © 2019 MacBook. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (MyDictionary)
-(NSDictionary *)deleteAllNullValue;

@end

NS_ASSUME_NONNULL_END
