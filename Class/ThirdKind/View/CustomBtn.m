//
//  CustomBtn.m
//  rainbowOnline
//
//  Created by MacBook on 26/03/2019.
//  Copyright © 2019 MacBook. All rights reserved.
//

#import "CustomBtn.h"

@implementation CustomBtn
// CustomButton 继承自UIButton
//#import "CustomButton.h"

//@implementation CustomButton
// 重写 initWithFrame: 方法；如果项目中使用到的按钮是通过xib生成的，则此处重写 awakeFromNib: 方法
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) { // 给按钮添加通知监听
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unHighlight) name:UIApplicationWillResignActiveNotification object:nil];
    }
    return self;
}

- (void)dealloc {  // 移除通知
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    BOOL inside = [super pointInside:point withEvent:event];
    if (inside && !self.isHighlighted && event.type == UIEventTypeTouches){
        self.highlighted = YES;
    }
    return inside;
}

- (void)unHighlight {
    self.highlighted = false;
}

@end

