//
//  LonnieCustomToast.m
//  SHAREBYLONNIE
//
//  Created by lonnie on 2017/9/21.
//  Copyright © 2017年 lonnie. All rights reserved.
//

#import "LonnieCustomToast.h"
#import "UIColor+Extension.h"

@implementation LonnieCustomToast

+(void)showMessage:(NSString *)message duration:(NSTimeInterval)time height:(ShowAlterType)type
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    UIView *showview =  [[UIView alloc]init];
    showview.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    showview.frame = CGRectMake(1, 1, 1, 1);
    showview.alpha = 1.0f;
    showview.layer.cornerRadius = 5.0f;
    showview.layer.masksToBounds = YES;
    [window addSubview:showview];
    
    UILabel *label = [[UILabel alloc]init];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16],
                                 NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    CGSize labelSize = [message boundingRectWithSize:CGSizeMake(207, 999)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:attributes context:nil].size;
    label.frame = CGRectMake(10,10, labelSize.width + 20, labelSize.height);
    label.text = message;
    label.textColor = [UIColor whiteColor];
    label.textAlignment = 1;
    label.numberOfLines = 0;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:16];
    [showview addSubview:label];
    
    CGFloat height = 0;
    switch (type) {
        case ShowAlterTypeCenter:
        {
            height = screenSize.height/2 - labelSize.height/2;
        }
            break;
        case ShowAlterTypeBottom:
        {
            height = screenSize.height - 100;
        }
            break;
            
        default:
            break;
    }
    
    showview.frame = CGRectMake((screenSize.width - labelSize.width - 20)/2,
                                height,
                                labelSize.width+40,
                                labelSize.height+20);
    [UIView animateWithDuration:time animations:^{
        showview.alpha = 0;
    } completion:^(BOOL finished) {
        [showview removeFromSuperview];
    }];
}

+(void)showMessage:(NSString *)message
{
    [self showMessage:message duration:2.5 height:ShowAlterTypeCenter];
}

+(UILabel *)addLine:(CGRect)rect
{
    UILabel *line = [[UILabel alloc]initWithFrame:rect];
    line.backgroundColor = [UIColor colorWithHexString:@"#f2f2f2"];
    return line;
}

@end
