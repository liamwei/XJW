//
//  BoxCell.m
//  rainbowOnline
//
//  Created by MacBook on 26/03/2019.
//  Copyright © 2019 MacBook. All rights reserved.
//

#import "BoxCell.h"
//#import "UIImageView+WebCache.h"
@interface BoxCell ()
@property (nonatomic,strong)UIImageView *img;
@property (nonatomic,strong)UILabel *lab;
@end

@implementation BoxCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (void)initUI {
//    self.layer
    UIImageView *img = [[UIImageView alloc]init];
//    WithImage: [UIImage imageNamed:[self.otherItemNameArr[indexPath.item] objectForKey:@"image"]]
    img.frame = CGRectMake((self.frame.size.width - 50)/2,(self.frame.size.height - 80)/2, 50, 50);
    [self.contentView addSubview:img];
    self.img = img;
    
    UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(img.frame), self.frame.size.width, 30)];
    lab.font = [UIFont systemFontOfSize:11];
    lab.textAlignment = NSTextAlignmentCenter;
//    lab.text = [self.otherItemNameArr[indexPath.item] objectForKey:@"name"];
    [self.contentView addSubview:lab];
    self.lab = lab;
}

- (void)setImaDic:(NSDictionary *)imaDic {
    _imaDic = imaDic;
    if (![[imaDic objectForKey:@"image"] containsString:@"http"]) {
        self.img.image = [UIImage imageNamed:[imaDic objectForKey:@"image"]];
    }else{
        self.img.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[imaDic objectForKey:@"image"]]]];
    }
    
//    [self.img sd_setImageWithURL: placeholderImage:[UIImage imageNamed:[imaDic objectForKey:@"image"]]];
    self.lab.text =  [imaDic objectForKey:@"name"];
}

@end
