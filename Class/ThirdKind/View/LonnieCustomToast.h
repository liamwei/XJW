//
//  LonnieCustomToast.h
//  SHAREBYLONNIE
//
//  Created by lonnie on 2017/9/21.
//  Copyright © 2017年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum : NSUInteger {
    ShowAlterTypeBottom,//底端
    ShowAlterTypeCenter,
    
} ShowAlterType;

@interface LonnieCustomToast : UIView
/*
 *  显示 message time  显示位置
 *
 */
+(void)showMessage:(NSString *)message duration:(NSTimeInterval)time height:(ShowAlterType)type;
/*
 *  显示 message
 *
 */
+(void)showMessage:(NSString *)message;
/*
 * 增加一根线
 *
 */
+(UILabel *)addLine:(CGRect)rect;

@end
