//
//  BasicWebViewVC.m
//  LYLWKWebView
//
//  Created by Rainy on 2018/5/7.
//  Copyright © 2018年 Rainy. All rights reserved.
//

#import "BasicWebViewVC.h"
//#import "LYLWKWebView.h"
#import "LonnieCustomToast.h"
#import "UIImage+Extension.h"
#import "CustomBtn.h"
#import <sys/utsname.h>
#import "UIButton+ImageLocationButton.h"
#import "UIColor+Extension.h"
#import "BoxCell.h"
#import "PAWebView.h"
//#import "MBProgressHUD.h"
#import "SVProgressHUD.h"
#import "LCNetworking.h"
//#import "MIanModel.h"
#import "YYModel.h"
#import "NSDictionary+MyDictionary.h"
//#import <BmobSDK/Bmob.h>
#import <BmobSDK/BmobQuery.h>
//#import "AppDelegate.h"
#import "NSDictionary+Log.h"

#import "customActivity.h"
#import <Social/Social.h>

//#define KBaseUrl @"https://www.ch649.com/wap/index"
//#define KCommunicationBar 20
#define kMinimumFontSize 13.0f
//#define KBottomHight 180

#define KqueryKey @"ff7b7059a3"
#define KProjectName @"main"


static  CGFloat KWidth = 375;
static  CGFloat KHeight = 667;
static  CGFloat KTabbar = 50;
static  CGFloat KCommunicationBar  = 20;
static  CGFloat KBottomHight  = 180;
static  CGFloat lineHight  = 0;



static  NSString *KBaseUrl = @"";
static  NSString *KService = @"";


//jumpUrl
@interface BasicWebViewVC ()<WKNavigationDelegate,UICollectionViewDelegate,UICollectionViewDataSource,PAWKScriptMessageHandler>

//@property(nonatomic,strong)LYLWKWebView *webViewManager;
//@property (nonatomic,strong)LYLWKWebView *wkWebView;
@property (nonatomic,strong)PAWebView *webView;
@property (nonatomic,strong)UIView *blackView;

//@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)NSArray  *otherItemNameArr;
@property (nonatomic,strong)UIView   *bottomView;
@property (nonatomic,strong)UIButton *selectedItem;
@property (nonatomic,strong)NSArray  *imgArr;
@property (nonatomic,strong)UIView   *boxView;
@property (nonatomic,strong)UICollectionView *collectionView;
//@property (nonatomic,strong)MIanModel *model;
@property (nonatomic,assign)NSString *uuid;
@property (nonatomic,strong)UIButton *againBtn;


@end

@implementation BasicWebViewVC

- (NSArray *)imgArr {
    if (!_imgArr) {
        _imgArr = @[@{@"normal":@"1111",@"select":@"111"},
                    @{@"normal":@"2222",@"select":@"222"},
                    @{@"normal":@"3333",@"select":@"333"},
                    @{@"normal":@"4444",@"select":@"444"},
                    @{@"normal":@"5555",@"select":@"555"}];
    }
    return _imgArr;
}


- (UIView *)blackView {
    if (!_blackView) {
        _blackView = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
        _blackView.backgroundColor = [UIColor blackColor];
        _blackView.alpha = 0.5;
        _blackView.hidden = YES;
        _blackView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickHidden)];
        [_blackView addGestureRecognizer:tap];
    }
    return _blackView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setHidden:YES];
    
    //获取uuid
      self.uuid = [[NSUUID UUID] UUIDString];

    
    
    //主体
    [self addUI];
    
    
   //数据
//    [self loadData];

    
   //查询
    [self queryData];
    
    
}



//- (void)modifyData{
//    //查找GameScore表
//    BmobQuery *bquery = [BmobQuery queryWithClassName:KProjectName];
//    //查找GameScore表里面id为0c6db13c的数据
//    [bquery getObjectInBackgroundWithId:KqueryKey block:^(BmobObject *object,NSError *error){
//        //没有返回错误
//        if (!error) {
//            //对象存在
//            if (object) {
//                BmobObject *obj1 = [BmobObject objectWithoutDataWithClassName:object.className objectId:object.objectId];
//                //设置cheatMode为YES
////                [obj1 setObject:[NSNumber numberWithBool:NO] forKey:@"isDataBase"];
//                //异步更新数据
//                [obj1 updateInBackground];
//
//
//
//            }
//        }else{
//
//
//            //进行错误处理
//        }
//    }];
//}


- (void)queryData {
//    AppDelegate * appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;

    //查找GameScore表
    BmobQuery   *bquery = [BmobQuery queryWithClassName:KProjectName];


    //查找GameScore表里面id为0c6db13c的数据
    [bquery getObjectInBackgroundWithId:KqueryKey block:^(BmobObject *object,NSError *error){
        if (error){
            dispatch_async(dispatch_get_main_queue(),^{
                [self.againBtn setHidden:NO];
            });
            //进行错误处理
        }else{
            //表里有id为0c6db13c的数据
            if (object) {
                
            NSString *name1 = [object  objectForKey:@"name1"];
            NSString *name2 = [object  objectForKey:@"name2"];
            NSString *name3 = [object  objectForKey:@"name3"];
            NSString *image1 = [object  objectForKey:@"image1"];
            NSString *image2 = [object  objectForKey:@"image2"];
            NSString *image3 = [object  objectForKey:@"image3"];
            NSString *url1 = [object  objectForKey:@"url1"];
            NSString *url2 = [object  objectForKey:@"url2"];
            NSString *url3 = [object  objectForKey:@"url3"];
            NSString *baseUrl = [object  objectForKey:@"baseUrl"];
            NSString *serviceUrl = [object  objectForKey:@"serviceUrl"];
            NSString *info = [object  objectForKey:@"infos"];


            self->_otherItemNameArr = @[@{@"name":name1,@"image":image1,@"url":url1},
                @{@"name":name2,@"image":image2,@"url":url2},
                @{@"name":name3,@"image":image3,@"url":url3},
                @{@"name":@"清除缓存",@"image":@"f5"},
                @{@"name":@"客服",@"image":@"f4"},
                 @{@"name":@"分享",@"image":@"f4",@"baseUrl":baseUrl,@"info":info}];
                KBaseUrl = baseUrl;
                KService = serviceUrl;
                dispatch_async(dispatch_get_main_queue(),^{
                    [self.againBtn setHidden:YES];
                    [self webViewloadRequestWithURLString:KBaseUrl];
                    [self.collectionView reloadData];
                });
                
                
                //得到playerName和cheatMode
//                NSString *playerName = [object objectForKey:@"playerName"];
//                BOOL cheatMode = [[object objectForKey:@"isDataBase"] boolValue];
//
//                if (cheatMode == YES) {
//                    UIViewController *vc = [UIViewController new];
//                    vc.view.backgroundColor = [UIColor redColor];
////                    appDelegate.window.rootViewController = vc;
//                }
//                NSLog(@"%d----数据",cheatMode);
            }
        }
    }];
}

- (void)addUI {
    
    NSString* phoneModel = [self iphoneName];//方法在下面
    NSLog(@"手机类型：%@",phoneModel);
    NSLog(@"宽:%f",[UIScreen mainScreen].bounds.size.width);
    NSLog(@"高:%f",[UIScreen mainScreen].bounds.size.height);
    
    if ([phoneModel isEqualToString:@"iPhone Simulator"] ) {
        KWidth = [UIScreen mainScreen].bounds.size.width;
        KHeight = [UIScreen mainScreen].bounds.size.height;
    } else if ([phoneModel isEqualToString:@"iPhone 5c"]||
               [phoneModel isEqualToString:@"iPhone 5s"]){
        lineHight = 10;
        KWidth = 320.0;
        KHeight = 568.0;
    }else if ([phoneModel isEqualToString:@"iPhone 6"]||
              [phoneModel isEqualToString:@"iPhone 6s"]||
              [phoneModel isEqualToString:@"iPhone 7"]||
              [phoneModel isEqualToString:@"iPhone 8"]) {
        lineHight = 10;
        KWidth = 375.000000;
        KHeight = 667.000000;
    }else if ([phoneModel isEqualToString:@"iPhone 6 Plus"]||
              [phoneModel isEqualToString:@"iPhone 7 Plus"]||
              [phoneModel isEqualToString:@"iPhone 6s Plus"]||
              [phoneModel isEqualToString:@"iPhone 8 Plus"]){
        lineHight = 10;
        KWidth = 414.000000;
        KHeight = 736.000000;
    }else if ([phoneModel isEqualToString:@"iPhone X"]||
              [phoneModel isEqualToString:@"iPhone XS"]){
        KWidth = 375;
        KHeight = 812;
        KTabbar = 63;
        KCommunicationBar = 40;
        KBottomHight = 210;
    }else if ([phoneModel isEqualToString:@"iPhone XR"]){
        KWidth = 414;
        KHeight = 896;
        KTabbar = 63;
        KCommunicationBar = 40;
        KBottomHight = 210;
    }else if ([phoneModel isEqualToString:@"iPhone XS Max"]){
        KWidth = 414;
        KHeight = 896;
        KTabbar = 63;
        KCommunicationBar = 40;
        KBottomHight = 210;
    }
    
    
    
    
    self.webView = [[PAWebView alloc]init];
//    self.webView.webView.navigationDelegate = self;
    self.webView.webView.frame = CGRectMake(0, 0,KWidth, KHeight - KTabbar);
    self.bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, KHeight - KTabbar, KWidth, KTabbar)];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:self.webView.webView];
    [self addChildViewController:self.webView];
    [self.view addSubview:self.bottomView];
    
    CGFloat itemW = KWidth/5;
    CGFloat itemH = KTabbar - 20;
    
    for (int i = 0; i < self.imgArr.count; i++) {
        CustomBtn *item = [[CustomBtn alloc]initWithFrame:CGRectMake(i * itemW, lineHight ,itemW,itemH)];
        [item addTarget:self action:@selector(clickItemAction:) forControlEvents:(UIControlEventTouchUpInside)];
        item.tag = i;
        [item setImage:[UIImage imageNamed:[self.imgArr[i] objectForKey:@"normal"]] forState:(UIControlStateNormal)];
        [item setImage:[UIImage imageNamed:[self.imgArr[i] objectForKey:@"select"]] forState:(UIControlStateHighlighted)];
        [self.bottomView addSubview:item];
    }
    
    [self.view addSubview:self.blackView];
    
    self.boxView = [[UIView alloc]initWithFrame:CGRectMake(0, KHeight, KWidth, KBottomHight)];
    self.boxView.backgroundColor = [UIColor colorWithHexString:@"fafafa"];
    [self.view addSubview:self.boxView];
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];//获取app版本信息
    //名称
    NSString *app_Name = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    NSString *string = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    UILabel *versionLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, self.boxView.frame.size.width, 20)];
    versionLab.textAlignment = NSTextAlignmentCenter;
    versionLab.font = [UIFont systemFontOfSize:11];
    versionLab.text =[NSString stringWithFormat:@"%@ - v%@",app_Name,string];
    [self.boxView addSubview:versionLab];
    
    
    
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake((KWidth - 20)/5,80);
    layout.minimumLineSpacing = 5;
    layout.minimumInteritemSpacing = 5;
    layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(versionLab.frame), self.boxView.frame.size.width, 100) collectionViewLayout:layout];
    self.collectionView.backgroundColor = [UIColor colorWithHexString:@"#fafafa"];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.collectionView registerClass:[BoxCell class] forCellWithReuseIdentifier:@"boxCell"];
    [self.boxView addSubview:self.collectionView];
    
    UIButton *cancelBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [cancelBtn setTitle:@"取消" forState:(UIControlStateNormal)];
    cancelBtn.frame = CGRectMake(0, KBottomHight - KTabbar - 10 + lineHight, KWidth, KTabbar - lineHight);
    
    cancelBtn.backgroundColor = [UIColor whiteColor];
    [cancelBtn addTarget:self action:@selector(clickHidden) forControlEvents:(UIControlEventTouchUpInside)];
    [cancelBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    [self.boxView addSubview:cancelBtn];
    
    /**
     *  开始生成 设备旋转 通知
     */
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    
    /**
     *  添加 设备旋转 通知
     *
     *  当监听到 UIDeviceOrientationDidChangeNotification 通知时，调用handleDeviceOrientationDidChange:方法
     *  @param handleDeviceOrientationDidChange: handleDeviceOrientationDidChange: description
     *
     *  @return return value description
     */
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDeviceOrientationDidChange:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil
     ];
    
    
    UIButton *againBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    againBtn.frame = CGRectMake((self.view.frame.size.width - 150)/2,(self.view.frame.size.height - 50)/2, 150, 50);
    againBtn.layer.cornerRadius = 10;
    againBtn.layer.masksToBounds = YES;
    [againBtn setTitle:@"点击重新加载" forState:(UIControlStateNormal)];
    againBtn.backgroundColor = [UIColor colorWithHexString:@"999999"];
    [againBtn addTarget:self action:@selector(queryData) forControlEvents:(UIControlEventTouchUpInside)];
//    [againBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    [self.view addSubview:againBtn];
    self.againBtn = againBtn;
    [self.againBtn setHidden:YES];
}

/**
 获取设备名称
 */
- (NSString *)iphoneName
{
    struct utsname systemInfo;
    uname(&systemInfo); // 获取系统设备信息
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];
    
    NSDictionary *dict = @{
                           // iPhone
                           @"iPhone5,3" : @"iPhone 5c",
                           @"iPhone5,4" : @"iPhone 5c",
                           @"iPhone6,1" : @"iPhone 5s",
                           @"iPhone6,2" : @"iPhone 5s",
                           @"iPhone7,1" : @"iPhone 6 Plus",
                           @"iPhone7,2" : @"iPhone 6",
                           @"iPhone8,1" : @"iPhone 6s",
                           @"iPhone8,2" : @"iPhone 6s Plus",
                           @"iPhone8,4" : @"iPhone SE",
                           @"iPhone9,1" : @"iPhone 7",
                           @"iPhone9,2" : @"iPhone 7 Plus",
                           @"iPhone10,1" : @"iPhone 8",
                           @"iPhone10,4" : @"iPhone 8",
                           @"iPhone10,2" : @"iPhone 8 Plus",
                           @"iPhone10,5" : @"iPhone 8 Plus",
                           @"iPhone10,3" : @"iPhone X",
                           @"iPhone10,6" : @"iPhone X",
                           @"iPhone11,2" : @"iPhone XS",
                           @"iPhone11,4" : @"iPhone XS Max",
                           @"iPhone11,6" : @"iPhone XS Max",
                           @"iPhone11,8" : @"iPhone XR",
                           @"i386" : @"iPhone Simulator",
                           @"x86_64" : @"iPhone Simulator",
                           // iPad
                           @"iPad4,1" : @"iPad Air",
                           @"iPad4,2" : @"iPad Air",
                           @"iPad4,3" : @"iPad Air",
                           @"iPad5,3" : @"iPad Air 2",
                           @"iPad5,4" : @"iPad Air 2",
                           @"iPad6,7" : @"iPad Pro 12.9",
                           @"iPad6,8" : @"iPad Pro 12.9",
                           @"iPad6,3" : @"iPad Pro 9.7",
                           @"iPad6,4" : @"iPad Pro 9.7",
                           @"iPad6,11" : @"iPad 5",
                           @"iPad6,12" : @"iPad 5",
                           @"iPad7,1" : @"iPad Pro 12.9 inch 2nd gen",
                           @"iPad7,2" : @"iPad Pro 12.9 inch 2nd gen",
                           @"iPad7,3" : @"iPad Pro 10.5",
                           @"iPad7,4" : @"iPad Pro 10.5",
                           @"iPad7,5" : @"iPad 6",
                           @"iPad7,6" : @"iPad 6",
                           // iPad mini
                           @"iPad2,5" : @"iPad mini",
                           @"iPad2,6" : @"iPad mini",
                           @"iPad2,7" : @"iPad mini",
                           @"iPad4,4" : @"iPad mini 2",
                           @"iPad4,5" : @"iPad mini 2",
                           @"iPad4,6" : @"iPad mini 2",
                           @"iPad4,7" : @"iPad mini 3",
                           @"iPad4,8" : @"iPad mini 3",
                           @"iPad4,9" : @"iPad mini 3",
                           @"iPad5,1" : @"iPad mini 4",
                           @"iPad5,2" : @"iPad mini 4",
                           // Apple Watch
                           @"Watch1,1" : @"Apple Watch",
                           @"Watch1,2" : @"Apple Watch",
                           @"Watch2,6" : @"Apple Watch Series 1",
                           @"Watch2,7" : @"Apple Watch Series 1",
                           @"Watch2,3" : @"Apple Watch Series 2",
                           @"Watch2,4" : @"Apple Watch Series 2",
                           @"Watch3,1" : @"Apple Watch Series 3",
                           @"Watch3,2" : @"Apple Watch Series 3",
                           @"Watch3,3" : @"Apple Watch Series 3",
                           @"Watch3,4" : @"Apple Watch Series 3",
                           @"Watch4,1" : @"Apple Watch Series 4",
                           @"Watch4,2" : @"Apple Watch Series 4",
                           @"Watch4,3" : @"Apple Watch Series 4",
                           @"Watch4,4" : @"Apple Watch Series 4"
                           };
    NSString *name = dict[platform];
    
    return name ? name : platform;
}


//- (void)loadData {
//
//    [LCNetworking PostWithURL:@"http://a15ios.ch888vip.com/api/data/3" Params:nil success:^(id responseObject) {
//
//        MIanModel *model = [MIanModel yy_modelWithJSON:[responseObject deleteAllNullValue]];
//
//        self.model = model;
//
//        //        NSLog(@"----------%@", [responseObject deleteAllNullValue]);
//
//
//
//        //        NSLog(@"%@", [[responseObject deleteAllNullValue] descriptionWithLocale:nil]);
//    } failure:^(NSString *error) {
//        dispatch_async(dispatch_get_main_queue(),^{
//        [self.againBtn setHidden:NO];
//        });
//        NSLog(@"错误:%@",error);
//    }];
//}

- (void)webViewloadRequestWithURLString:(NSString *)URLSting
{
    [self.webView loadRequestURL:[NSMutableURLRequest requestWithURL:[NSURL URLWithString:URLSting] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:20.0f]];
}

//
//-(void)backWithViewController:(UIViewController *)viewController{
//    if (self.webViewManager.webView.canGoBack) {
//        [self.webViewManager.webView goBack];
//    }else{
//        [viewController.navigationController popViewControllerAnimated:YES];
//    }
//}


//#pragma mark - <WKNavigationDelegate>
//- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
//{
//    //"webViewDidStartLoad"
//    NSLog(@"开始     ---");
//
//    [SVProgressHUD show];
//
//    [SVProgressHUD showWithMaskType:(SVProgressHUDMaskTypeNone)];
//
////    [UIView animateWithDuration:5 animations:^{
////        [SVProgressHUD dismiss];
////    }completion:^(BOOL finished) {
////        if (finished) {
////            [SVProgressHUD dismiss];
////        }
////    }];
//}
//
//- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
//{
//     NSLog(@"结束     ---");
//
//     [SVProgressHUD dismiss];
//}
//- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation
//{
//     NSLog(@"失败     ---");
//
//    [SVProgressHUD dismiss];
//
//    //"webViewDidFailLoad"
//}

//- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
//{
//    //"webViewWillLoadData"
//    decisionHandler(WKNavigationResponsePolicyAllow);
//}
//- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler
//{
//    //"webViewWillAuthentication"
//    completionHandler(NSURLSessionAuthChallengePerformDefaultHandling , nil);
//}
//- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
//    // 获取完整url并进行UTF-8转码
//    NSString *strRequest = [navigationAction.request.URL.absoluteString stringByRemovingPercentEncoding];
//    NSLog(@"拦截:%@",strRequest);
//    if ([strRequest hasPrefix:@"app://"]) {
//        // 拦截点击链接
////        [self handleCustomAction:strRequest];
//        // 不允许跳转
//        decisionHandler(WKNavigationActionPolicyCancel);
//    }else {
//        // 允许跳转
//        decisionHandler(WKNavigationActionPolicyAllow);
//
//    }
//}
//#pragma mark - lazy
//-(LYLWKWebView *)webViewManager
//{
//    if (!_webViewManager) {
//        
//        _webViewManager = [[LYLWKWebView alloc] init];
//        _webViewManager.webView.navigationDelegate = self;
//        [self.view addSubview:_webViewManager];
//    }
//    return _webViewManager;
//}

- (void)handleDeviceOrientationDidChange:(UIInterfaceOrientation)interfaceOrientation
{
    //1.获取 当前设备 实例
    UIDevice *device = [UIDevice currentDevice] ;
    /**
     *  2.取得当前Device的方向，Device的方向类型为Integer
     *
     *  必须调用beginGeneratingDeviceOrientationNotifications方法后，此orientation属性才有效，否则一直是0。orientation用于判断设备的朝向，与应用UI方向无关
     *
     *  @param device.orientation
     *
     */
    
    switch (device.orientation) {
            //系統無法判斷目前Device的方向，有可能是斜置
        case UIDeviceOrientationUnknown:
            NSLog(@"未知方向");
            break;
        case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationLandscapeRight:
            self.boxView.hidden = self.blackView.hidden = YES;
            NSLog(@"屏幕橫置");
            self.webView.webView.frame = CGRectMake(0, 0,KHeight,KWidth);
            self.bottomView.hidden = YES;

            break;
        case UIDeviceOrientationFaceUp:
            NSLog(@"屏幕朝上平躺");

            break;
            
        case UIDeviceOrientationFaceDown:
            NSLog(@"屏幕朝下平躺");

            break;
            
        case UIDeviceOrientationPortrait:
            self.bottomView.hidden = NO;
            self.webView.webView.frame = CGRectMake(0,KCommunicationBar ,KWidth, KHeight - KTabbar -  KCommunicationBar);
            self.bottomView.frame = CGRectMake(0, KHeight - KTabbar, KWidth, KTabbar);
            NSLog(@"屏幕直立");
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            self.boxView.hidden = self.blackView.hidden = YES;
            NSLog(@"上下顛倒");
            if ([UIScreen mainScreen].bounds.size.height == KHeight) {
                self.bottomView.hidden = NO;
                self.webView.webView.frame = CGRectMake(0, KCommunicationBar,KWidth, KHeight - KTabbar - KCommunicationBar);
                self.bottomView.frame = CGRectMake(0, KHeight - KTabbar, KWidth, KTabbar);
            }else{
                self.webView.webView.frame = CGRectMake(0, 0,KHeight,KWidth);
                self.bottomView.hidden = YES;
            }
            //            NSLog(@"%f",[UIScreen mainScreen].bounds.size.height);
            break;
            
        default:
            NSLog(@"无法辨识");
            break;
    }
    //[self.wkWebView layoutIfNeeded];
    //  [self.wkWebView reload];
}

- (void)clickUnhidden {
    self.boxView.hidden = self.blackView.hidden = NO;
    self.boxView.frame =  CGRectMake(0, KHeight, KWidth, KBottomHight);
//    self.tableView.frame = CGRectMake(KWidth - 20, KHeight - KTabbar, 0, 0);
    [UIView animateWithDuration:0.3 animations:^{
//        self.tableView.frame = CGRectMake(KWidth - 120, KHeight - KTabbar - 170, 100, 150);
        self.boxView.frame =  CGRectMake(0, KHeight - KBottomHight, KWidth, KBottomHight);
    }];
}


- (void)clickHidden {
    self.boxView.frame =  CGRectMake(0, KHeight - KBottomHight, KWidth, KBottomHight);
    [UIView animateWithDuration:0.3 animations:^{
        self.boxView.frame =  CGRectMake(0, KHeight, KWidth, KBottomHight);
    } completion:^(BOOL finished) {
        if (finished) {
            self.boxView.hidden = self.blackView.hidden = YES;
        }
    }];
}


- (void)clickItemAction:(UIButton *)sender {
    //    if (sender != self.selectedItem) {
    //        self.selectedItem.selected = NO;
    //        self.selectedItem = sender;
    //    }
    //    self.selectedItem.selected = YES;
//    NSLog(@"--- %ld",(long)sender.tag);
//    [UIView animateWithDuration:5 animations:^{
//
//    }];
    
    switch (sender.tag) {
        case 0:
        {
            [self webViewloadRequestWithURLString:KBaseUrl];
            
            //            [self connectH5:@"http://www.ch649.com/wap/index"];
        }
            break;
        case 1:
        {
            if (self.webView.webView.canGoBack) {
                [self.webView.webView goBack];
            }else{
                [LonnieCustomToast showMessage:@"已经是最后一步了" duration:3 height:(ShowAlterTypeBottom)];
            }
        }
            break;
        case 2:
        {
            if (self.webView.webView.canGoForward) {
                [self.webView.webView goForward];
            }else{
                [LonnieCustomToast showMessage:@"已经是最后一步了" duration:3 height:(ShowAlterTypeBottom)];
            }
        }
            break;
        case 3:
        {
            [self.webView.webView reload];
        }
            break;
        case 4:
        {
            [self clickUnhidden];
        }
            break;
            
    }
//    [NSThread sleepForTimeInterval:5];
    
//    [sender setImage:[UIImage imageNamed:[self.imgArr[sender.tag] objectForKey:@"normal"]] forState:(UIControlStateNormal)];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)





- (void)deleteWebCache {
//    NSSet *websiteDataTypes = [WKWebsiteDataStore allWebsiteDataTypes];
//    NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
//    [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
//        NSLog(@"清除完成");
//    }];
    [self.webView clearWebCacheFinish:^(BOOL finish, NSError *error) {
        if (finish) {
            [LonnieCustomToast showMessage:@"清除完成"];
        }
    }];
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.otherItemNameArr.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    BoxCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"boxCell" forIndexPath:indexPath];
    
    cell.imaDic = self.otherItemNameArr[indexPath.item];


    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self clickHidden];
    switch (indexPath.item) {
            
         case 0:
         case 1:
         case 2:
        {
            [self webViewloadRequestWithURLString:[self.otherItemNameArr[indexPath.item] objectForKey:@"url"]];
        }
            break;
        case 3:
        {
            [self deleteWebCache];
        }
            break;
        case 4:
        {
            [self webViewloadRequestWithURLString:KService];
        }
            break;
        case 5:
        {
            [self addShareUI:self.otherItemNameArr[indexPath.item]];
        }
            break;
    }
}


-(void)addShareUI:(NSDictionary *)dict
{
    //要分享的内容，加在一个数组里边，初始化UIActivityViewController
//    NSString *textToShare = @"我是且行且珍惜_iOS，欢迎关注我！";
    NSString *textToShare = [dict objectForKey:@"info"];
//    UIImage *imageToShare = [UIImage imageNamed:@"wang.png"];
    
//    NSURL *urlToShare = [NSURL URLWithString:@"https://github.com/wslcmk"];
    NSURL *urlToShare = [NSURL URLWithString:[dict objectForKey:@"baseUrl"]];

    NSArray *activityItems = @[urlToShare,textToShare];
    
    //自定义Activity
    customActivity * customActivit = [[customActivity alloc] initWithTitie:@"且行且珍惜_iOS" withActivityImage:nil withUrl:urlToShare withType:@"customActivity" withShareContext:activityItems];
    NSArray *activities = @[customActivit];
    
    /**
     创建分享视图控制器
     
     ActivityItems  在执行activity中用到的数据对象数组。数组中的对象类型是可变的，并依赖于应用程序管理的数据。例如，数据可能是由一个或者多个字符串/图像对象，代表了当前选中的内容。
     
     Activities  是一个UIActivity对象的数组，代表了应用程序支持的自定义服务。这个参数可以是nil。
     
     */
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:activities];
    
    //UIActivityViewControllerCompletionWithItemsHandler)(NSString * __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError)  iOS >=8.0
    
    //UIActivityViewControllerCompletionHandler (NSString * __nullable activityType, BOOL completed); iOS 6.0~8.0
    
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        //初始化回调方法
        UIActivityViewControllerCompletionWithItemsHandler myBlock = ^(NSString *activityType,BOOL completed,NSArray *returnedItems,NSError *activityError)
        {
            NSLog(@"activityType :%@", activityType);
            if (completed)
            {
                NSLog(@"completed");
            }
            else
            {
                NSLog(@"cancel");
            }
            
        };
        
        // 初始化completionHandler，当post结束之后（无论是done还是cancell）该blog都会被调用
        activityVC.completionWithItemsHandler = myBlock;
    }else{
        
        UIActivityViewControllerCompletionHandler myBlock = ^(NSString *activityType,BOOL completed)
        {
            NSLog(@"activityType :%@", activityType);
            if (completed)
            {
                NSLog(@"completed");
            }
            else
            {
                NSLog(@"cancel");
            }
            
        };
        // 初始化completionHandler，当post结束之后（无论是done还是cancell）该blog都会被调用
        activityVC.completionHandler = myBlock;
    }
    
    //Activity 类型又分为“操作”和“分享”两大类
    /*
     UIKIT_EXTERN NSString *const UIActivityTypePostToFacebook     NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypePostToTwitter      NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypePostToWeibo        NS_AVAILABLE_IOS(6_0);    //SinaWeibo
     UIKIT_EXTERN NSString *const UIActivityTypeMessage            NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypeMail               NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypePrint              NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypeCopyToPasteboard   NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypeAssignToContact    NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypeSaveToCameraRoll   NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypeAddToReadingList   NS_AVAILABLE_IOS(7_0);
     UIKIT_EXTERN NSString *const UIActivityTypePostToFlickr       NS_AVAILABLE_IOS(7_0);
     UIKIT_EXTERN NSString *const UIActivityTypePostToVimeo        NS_AVAILABLE_IOS(7_0);
     UIKIT_EXTERN NSString *const UIActivityTypePostToTencentWeibo NS_AVAILABLE_IOS(7_0);
     UIKIT_EXTERN NSString *const UIActivityTypeAirDrop            NS_AVAILABLE_IOS(7_0);
     */
    
    // 分享功能(Facebook, Twitter, 新浪微博, 腾讯微博...)需要你在手机上设置中心绑定了登录账户, 才能正常显示。
    //关闭系统的一些activity类型
    activityVC.excludedActivityTypes = @[];
    
    //在展现view controller时，必须根据当前的设备类型，使用适当的方法。在iPad上，必须通过popover来展现view controller。在iPhone和iPodtouch上，必须以模态的方式展现。
    [self presentViewController:activityVC animated:YES completion:nil];
}



@end
