//
//  LL_CYLPlusButtonSubclass.h
//  XJW
//
//  Created by John on 2019/7/27.
//  Copyright © 2019 Liam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CYLPlusButton.h>
NS_ASSUME_NONNULL_BEGIN

@interface LL_CYLPlusButtonSubclass : CYLPlusButton <CYLPlusButtonSubclassing>

@end

NS_ASSUME_NONNULL_END
