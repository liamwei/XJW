//
//  TTMDetailViewController.m
//  AnimatedTransitionGallery
//
//  Created by shuichi on 3/10/14.
//  Copyright (c) 2014 Shuichi Tsutsumi. All rights reserved.
//

#import "TTMDetailViewController.h"


@interface TTMDetailViewController ()
@end


@implementation TTMDetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem description];
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    
    UIColor *startColor = [UIColor colorWithRed:1.
                                          green:149./255.
                                           blue:0.
                                          alpha:1.];
    UIColor *endColor   = [UIColor colorWithRed:1.
                                          green:94./255.
                                           blue:58./255.
                                          alpha:1.];

    gradient.colors = @[(id)startColor.CGColor, (id)endColor.CGColor];
    [self.view.layer insertSublayer:gradient atIndex:0];

    self.navigationController.title = [self.detailItem description];
    [self.view addSubview:self.backImageV];
    [self.view addSubview:self.detailDescriptionLabel];

    [self configureView];
}



-(UILabel *)detailDescriptionLabel
{
    if (!_detailDescriptionLabel) {
        _detailDescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, kScreenHeight/2 -44, kScreenWidth, 44)];
        _detailDescriptionLabel.textColor     = [UIColor cyl_systemGreenColor];
        _detailDescriptionLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _detailDescriptionLabel;
}

-(UIImageView *)backImageV
{
    if (!_backImageV) {
        _backImageV = [[UIImageView alloc] initWithFrame:self.view.bounds];
        _backImageV.image = [UIImage imageNamed:@"elcaptan"];
    }
    return _backImageV;
}








- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
