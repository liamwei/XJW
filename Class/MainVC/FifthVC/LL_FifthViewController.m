//
//  LL_FifthViewController.m
//  XJW
//
//  Created by John on 2019/7/27.
//  Copyright © 2019 Liam. All rights reserved.
//  https://github.com/shu223/AnimatedTransitionGallery

#import "LL_FifthViewController.h"
//#import "LL_MainTabbarViewController.H"
//#import "LL_MainRootViewController.h"
//#import <MJRefresh/MJRefresh.h>

#import "LL_DetailsViewController.h"
#import <MJRefresh/MJRefresh.h>


#import "TTMDetailViewController.h"
#import "HUTransitionAnimator.h"
#import "ATCAnimatedTransitioning.h"
#import "ATCAnimatedTransitioningFade.h"
#import "ATCAnimatedTransitioningBounce.h"
#import "LCZoomTransition.h"
#import "CEReversibleAnimationController.h"
#import "ADTransitionController.h"
#import "KWTransitionHelper.h"
#import "DMBaseTransition.h"
#import "HFAnimator.h"
#import "HFDynamicAnimator.h"
#import "FlipTransition.h"
#import "CoreImageTransition.h"
#import "CoreImageBlurTransition.h"
#import "CoreImageMotionBlurTransition.h"


#define kClassNameForCoreImageTransition @"CoreImageTransition"


static NSString * CellIdentifier = @"Cell5";

@interface LL_FifthViewController ()<UINavigationControllerDelegate>
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSString *transitionClassName;
@property (nonatomic, strong) id animator;

@end

@implementation LL_FifthViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"过渡动画";    //✅sets navigation bar title.The right way to set the title of the navigation
    self.tabBarItem.title = @"加号";   //❌sets tab bar title. Even the `tabBarItem.title` changed, this will be ignored in tabbar.
    [self showMainData];
    //self.title = @"我的1";                //❌sets both of these. Do not do this‼️‼️ This may cause something strange like this : http://i68.tinypic.com/282l3x4.jpg .
    //    [self.navigationController.tabBarItem setBadgeValue:@"3"];
    __weak __typeof(self) weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //Call this Block When enter the refresh status automatically
        NSUInteger delaySeconds = 1;
        dispatch_time_t when = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delaySeconds * NSEC_PER_SEC));
        dispatch_after(when, dispatch_get_main_queue(), ^{
            [weakSelf.tableView.mj_header endRefreshing];
        });
    }];
    self.tableView.tableFooterView = [UIView new];

}


-(void)showMainData
{
    UIView *backgroundView = [[UIView alloc] initWithFrame:self.tableView.bounds];
       CAGradientLayer *gradient = [CAGradientLayer layer];
       gradient.frame = backgroundView.bounds;
       UIColor *startColor = [UIColor colorWithRed: 90./255.
                                             green:200./255.
                                              blue:251./255.
                                             alpha:1.0];
       UIColor *endColor   = [UIColor colorWithRed: 82./255.
                                             green:237./255.
                                              blue:199./255.
                                             alpha:1.0];
       
       gradient.colors = @[(id)startColor.CGColor, (id)endColor.CGColor];
       [backgroundView.layer addSublayer:gradient];
       self.tableView.backgroundView = backgroundView;

       self.navigationController.delegate = self;
       
       self.items = @[
                      @"HUTransitionVerticalLinesAnimator",
                      @"HUTransitionHorizontalLinesAnimator",
                      @"HUTransitionGhostAnimator",
                      @"ZBFallenBricksAnimator",
                      [NSString stringWithFormat:@"%@%@", kClassNameForCoreImageTransition, kCoreImageTransitionTypeNameBoxBlur],
                      [NSString stringWithFormat:@"%@%@", kClassNameForCoreImageTransition, kCoreImageTransitionTypeNameMotionBlur],
                      [NSString stringWithFormat:@"%@%@", kClassNameForCoreImageTransition, kCoreImageTransitionTypeNameCopyMachine],
                      [NSString stringWithFormat:@"%@%@", kClassNameForCoreImageTransition, kCoreImageTransitionTypeNameDisintegrateWithMask],
                      [NSString stringWithFormat:@"%@%@", kClassNameForCoreImageTransition, kCoreImageTransitionTypeNameDissolve],
                      [NSString stringWithFormat:@"%@%@", kClassNameForCoreImageTransition, kCoreImageTransitionTypeNameFlash],
                      [NSString stringWithFormat:@"%@%@", kClassNameForCoreImageTransition, kCoreImageTransitionTypeNameMod],
                      [NSString stringWithFormat:@"%@%@", kClassNameForCoreImageTransition, kCoreImageTransitionTypeNamePageCurl],
                      [NSString stringWithFormat:@"%@%@", kClassNameForCoreImageTransition, kCoreImageTransitionTypeNamePageCurlWithShadow],
                      [NSString stringWithFormat:@"%@%@", kClassNameForCoreImageTransition, kCoreImageTransitionTypeNameRipple],
                      [NSString stringWithFormat:@"%@%@", kClassNameForCoreImageTransition, kCoreImageTransitionTypeNameSwipe],
                      @"ATCAnimatedTransitioningFade",
                      @"ATCAnimatedTransitioningBounce",
                      @"ATCAnimatedTransitioningSquish",
                      @"ATCAnimatedTransitioningFloat",
                      @"LCZoomTransition",
                      @"ADBackFadeTransition",
                      @"ADCarrouselTransition",
                      @"ADCrossTransition",
                      @"ADCubeTransition",
                      @"ADFadeTransition",
                      @"ADFlipTransition",
                      @"ADFoldTransition",
                      @"ADGhostTransition",
                      @"ADGlueTransition",
                      @"ADModernPushTransition",
                      @"ADPushRotateTransition",
                      @"ADScaleTransition",
                      @"ADSlideTransition",
                      @"ADSwapTransition",
                      @"ADSwipeFadeTransition",
                      @"ADSwipeTransition",
                      @"ADZoomTransition",
                      @"CECardsAnimationController",
                      @"CECrossfadeAnimationController",
                      @"CECubeAnimationController",
                      @"CEExplodeAnimationController",
                      @"CEFlipAnimationController",
                      @"CEFoldAnimationController",
                      @"CENatGeoAnimationController",
                      @"CEPortalAnimationController",
                      @"CETurnAnimationController",
                      KWTransitionStyleNameRotateFromTop,
                      KWTransitionStyleNameFadeBackOver,
                      KWTransitionStyleNameBounceIn,
                      KWTransitionStyleNameDropOut,
                      KWTransitionStyleNameStepBackScroll,
                      KWTransitionStyleNameStepBackSwipe,
                      KWTransitionStyleNameUp,
                      KWTransitionStyleNamePushUp,
                      KWTransitionStyleNameFall,
                      KWTransitionStyleNameSink,
                      @"DMAlphaTransition",
                      @"DMScaleTransition",
                      @"DMSlideTransition",
                      @"HFAnimator",
                      @"HFDynamicAnimator",
                      @"BouncePresentTransition",
                      @"FlipTransition",
                      @"ShrinkDismissTransition",
                      ];
}







- (void)refresh {
    [self.tableView.mj_header beginRefreshing];
    NSUInteger delaySeconds = 1;
    dispatch_time_t when = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delaySeconds * NSEC_PER_SEC));
    dispatch_after(when, dispatch_get_main_queue(), ^{
        [self.tableView.mj_header endRefreshing];
    });
}
#pragma mark - Methods

- (void)configureCell:(UITableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath {
    cell.textLabel.text = [NSString stringWithFormat:@"🌛%@🌜", self.items[indexPath.row]];
//    [cell.detailTextLabel setText:[NSString stringWithFormat:@" 🌈👉 %@ 🌛 🌜%@🍓", self.tabBarItem.title, @(indexPath.row)]];

}

#pragma mark - Table view

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [self configureCell:cell forIndexPath:indexPath];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self cyl_showBadgeValue:[NSString stringWithFormat:@"%@", @(indexPath.row)] animationType:CYLBadgeAnimationTypeNone];
    
    TTMDetailViewController *vc = [[TTMDetailViewController alloc] init];
    self.transitionClassName = self.items[indexPath.row];

    [vc setDetailItem:self.items[indexPath.row]];
    [self.navigationController pushViewController:vc animated:YES];
}

//- (void)testPush {
//    UIViewController *viewController = [[UIViewController alloc] init];
//    viewController.view.backgroundColor = [UIColor redColor];
//    [self.navigationController pushViewController:viewController animated:YES];
//}






// =============================================================================
#pragma mark - UINavigationControllerDelegate

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC
{
    self.animator = nil;

    if (NSClassFromString(self.transitionClassName)) {
        
        Class aClass = NSClassFromString(self.transitionClassName);
        self.animator = [[aClass alloc] init];
    }
    // only for KWTransition
    else if ([self.transitionClassName hasPrefix:@"KWTransition"]) {
        
        KWTransition *transition = [[KWTransition alloc] init];
        transition.style = [KWTransitionHelper styleForTransitionName:self.transitionClassName];

        self.animator = transition;
    }
    // only for CoreImageTransition
    else if ([self.transitionClassName hasPrefix:kClassNameForCoreImageTransition]) {

        NSString *typeName = [self.transitionClassName stringByReplacingOccurrencesOfString:kClassNameForCoreImageTransition
                                                                                 withString:@""];
        
        CoreImageTransition *transition;
        if (
            [typeName isEqualToString:kCoreImageTransitionTypeNameBoxBlur] ||
            [typeName isEqualToString:kCoreImageTransitionTypeNameDiscBlur] ||
            [typeName isEqualToString:kCoreImageTransitionTypeNameGaussianBlur]
            )
        {
            transition = [CoreImageBlurTransition new];
        }
        else if ([typeName isEqualToString:kCoreImageTransitionTypeNameMotionBlur])
        {
            transition = [CoreImageMotionBlurTransition new];
        }
        else
        {
            transition = [CoreImageTransition new];
        }
        [transition setTransitionTypeWithName:typeName];
        
        self.animator = transition;
    }
    
    if (self.animator) {

        [self setupAnimatorForOperation:operation];
    }
    
    return self.animator;
}


// =============================================================================
#pragma mark - Private

// setup for each OSS
- (void)setupAnimatorForOperation:(UINavigationControllerOperation)operation
{
    // HUAnimator
    // DMCustomTransitions
    if (
        [self.animator isKindOfClass:[CoreImageTransition class]] ||
        [self.animator isKindOfClass:[HUTransitionAnimator class]] ||
        [self.animator isKindOfClass:[DMBaseTransition class]] ||
        [self.animator isKindOfClass:[HFAnimator class]] ||
        [self.animator isKindOfClass:[HFDynamicAnimator class]]
        )
    {
        [self.animator setPresenting:(operation == UINavigationControllerOperationPush)];
    }
    // Animated-Transition-Collection
    else if ([self.animator isKindOfClass:[ATCAnimatedTransitioning class]]) {
        
        [self.animator setDuration:1.0];
        [self.animator setIsPush:(operation != UINavigationControllerOperationPop)];
        
        if (![self.animator isKindOfClass:[ATCAnimatedTransitioningFade class]] &&
            ![self.animator isKindOfClass:[ATCAnimatedTransitioningBounce class]])
        {
            [self.animator setDismissal:(operation == UINavigationControllerOperationPop)];
        }
        
        if (operation == UINavigationControllerOperationPush) {
            
            [(ATCAnimatedTransitioning *)self.animator setDirection:ATCTransitionAnimationDirectionRight];
        }
        else {
            [(ATCAnimatedTransitioning *)self.animator setDirection:ATCTransitionAnimationDirectionLeft];
        }
    }
    // LCZoomTransition
    else if ([self.animator isKindOfClass:[LCZoomTransition class]]) {
        
        [(LCZoomTransition *)self.animator setTransitionDuration:0.5];
        [(LCZoomTransition *)self.animator setOperation:operation];
    }
    // VCTransitionsLibrary
    // FlipTransition
    else if ([self.animator isKindOfClass:[CEReversibleAnimationController class]] ||
             [self.animator isKindOfClass:[FlipTransition class]]) {
        
        [self.animator setReverse:(operation == UINavigationControllerOperationPop)];
    }
    // ADTransition
    else if ([self.animator isKindOfClass:[ADTransition class]]) {
        
        ADTransition *transition = self.animator;
        
        Class aClass = [transition class];
        
        if ([transition respondsToSelector:@selector(initWithDuration:orientation:sourceRect:)]) {
            
            transition = [[aClass alloc] initWithDuration:0.5f
                                              orientation:ADTransitionRightToLeft
                                               sourceRect:self.tableView.bounds];
        }
        else if ([transition respondsToSelector:@selector(initWithDuration:sourceRect:)]) {
            
            transition = [[aClass alloc] initWithDuration:0.5f
                                               sourceRect:self.tableView.bounds];
        }
        else {
            
            transition = [[aClass alloc] initWithDuration:0.5f];
        }
        
        self.animator = [[ADTransitioningDelegate alloc] initWithTransition:transition];
    }
    // KWTransition
    else if ([self.animator isKindOfClass:[KWTransition class]]) {
        
        if (operation == UINavigationControllerOperationPush) {
            
            [(KWTransition *)self.animator setAction:KWTransitionStepPresent];
        }
        else {
            [(KWTransition *)self.animator setAction:KWTransitionStepDismiss];
        }
        
        if ([(KWTransition *)self.animator style] == KWTransitionStyleSink) {
            [(KWTransition *)self.animator setSettings:KWTransitionSettingDirectionDown];
        }
    }
}


// =============================================================================
#pragma mark - IBAction

- (IBAction)pop:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}




- (void)dealloc {
    NSLog(@"🔴类名与方法名：%@（在第%@行），描述：%@", @(__PRETTY_FUNCTION__), @(__LINE__), @"");
}

@end
