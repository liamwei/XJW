//
//  LL_MeiziPicturesViewController.m
//  XJW
//
//  Created by John on 2019/12/27.
//  Copyright © 2019 Liam. All rights reserved.
//

#import "LL_MeiziPicturesViewController.h"
#import "DemoVC1Cell.h"
#import "UIImageView+WebCache.h"
#import "XHWebImageAutoSize.h"

static NSString *const cellId = @"DemoVC1Cell";

@interface LL_MeiziPicturesViewController () <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic , strong) UITableView *tableView;

@property (nonatomic , strong) NSArray *dataArray;

@end

@implementation LL_MeiziPicturesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    
    [self.view addSubview:self.tableView];
    
}

-(void)initData{
    NSData *JSONData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"image01" ofType:@"json"]];
    NSDictionary *json =  [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingAllowFragments error:nil];
    self.dataArray = json[@"data"];
}

-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight -64)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
//        _tableView.tableHeaderView = [[UIView alloc] init];
        _tableView.tableFooterView = [[UIView alloc] init];
//        _tableView.estimatedRowHeight = 44.0;
//        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        _tableView.scrollsToTop = YES;
    }
    return _tableView;
}
-(NSArray *)dataArray{
    if(!_dataArray){
        _dataArray = [NSArray array];
    }
    return _dataArray;
}
#pragma mark-tableView
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *url = self.dataArray[indexPath.row];
    
    /**
     *  参数1:图片URL
     *  参数2:imageView 宽度
     *  参数3:预估高度,(此高度仅在图片尚未加载出来前起作用,不影响真实高度)
     */
    return [XHWebImageAutoSize imageHeightForURL:[NSURL URLWithString:url] layoutWidth:[UIScreen mainScreen].bounds.size.width-16 estimateHeight:200];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DemoVC1Cell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[DemoVC1Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSString *url = self.dataArray[indexPath.row];
    [cell.myImageV sd_setImageWithURL:[NSURL URLWithString:url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        CGSize newSize = [self imageCompressForWidth:image.size targetWidth:(kScreenWidth -20)];
        [cell.myImageV setFrame:CGRectMake(10, 10, kScreenWidth - 20, newSize.height)];

        /** 缓存image size */
        [XHWebImageAutoSize storeImageSize:image forURL:imageURL completed:^(BOOL result) {
            /** reload  */
            if(result)  {
                [tableView  xh_reloadDataForURL:imageURL];
            }
        }];
    }];
    return cell;
}

//指定宽度按比例缩放
- (CGSize) imageCompressForWidth:(CGSize )imageSize targetWidth:(CGFloat)defineWidth
{
    if (imageSize.width == defineWidth) {
        return imageSize;
      }
    if (imageSize.height > 0) {
        float scale  = imageSize.width/defineWidth;
        float height = imageSize.height / scale;
        return CGSizeMake(defineWidth, height);
    }else{
        return CGSizeZero;
    }
}


@end
