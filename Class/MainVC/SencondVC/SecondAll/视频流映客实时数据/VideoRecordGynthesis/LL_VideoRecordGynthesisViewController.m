//
//  LL_VideoRecordGynthesisViewController.m
//  XJW
//
//  Created by John on 2020/1/17.
//  Copyright © 2020 Liam. All rights reserved.
//

#import "LL_VideoRecordGynthesisViewController.h"
#import "VideoCameraView.h"

@interface LL_VideoRecordGynthesisViewController ()<VideoCameraDelegate>
//@property (nonatomic ,strong) UITextField* configWidth;
//@property (nonatomic ,strong) UITextField* configHight;
//@property (nonatomic ,strong) UITextField* configBit;
//@property (nonatomic ,strong) UITextField* configFrameRate;
//@property (nonatomic ,strong) UIButton* okBtn;

@property (nonatomic ,strong) VideoCameraView* videoCameraView;
@end

@implementation LL_VideoRecordGynthesisViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.videoCameraView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.cyl_navigationBarHidden = YES;
    self.navigationController.navigationBarHidden = YES;

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    self.cyl_navigationBarHidden = NO;
    self.navigationController.navigationBarHidden = NO;

}


-(VideoCameraView *)videoCameraView
{
    if (!_videoCameraView) {
        int width,hight,bit,framRate;
        width = 720;
        hight = 1280;
        bit = 2500000;
        framRate = 30;
        _videoCameraView =[[VideoCameraView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        _videoCameraView.delegate = self;
        _videoCameraView.width = [NSNumber numberWithInteger:width];
        _videoCameraView.hight = [NSNumber numberWithInteger:hight];
        _videoCameraView.bit = [NSNumber numberWithInteger:bit];
        _videoCameraView.frameRate = [NSNumber numberWithInteger:framRate];
        typeof(self) __weak weakself = self;
        _videoCameraView.backToHomeBlock = ^(){
            [weakself.navigationController dismissViewControllerAnimated:NO completion:nil];
       };
    }
    return _videoCameraView;
}

-(void)presentCor:(UIViewController *)cor{
  [self presentViewController:cor animated:YES completion:nil];
}

-(void)pushCor:(UIViewController *)cor{
  [self.navigationController pushViewController:cor animated:YES];
}

- (BOOL)prefersStatusBarHidden{
  return YES;
}

-(void)dealloc{
    NSLog(@"%@释放了",self.class);
}






@end
