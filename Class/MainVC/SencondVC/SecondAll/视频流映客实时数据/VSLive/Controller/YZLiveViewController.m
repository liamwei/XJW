
//
//  YZLiveViewController.m
//  YZLiveApp
//
//  Created by yz on 16/8/29.
//  Copyright © 2016年 yz. All rights reserved.
//

#import "YZLiveViewController.h"
#import <IJKMediaFramework/IJKMediaFramework.h>
#import "YZLiveItem.h"
#import "YZCreatorItem.h"
#import <UIImageView+WebCache.h>
#import "LL_VideoStreaming.h"

@interface YZLiveViewController ()

@property (nonatomic, strong) UIImageView *imageView;

@property (nonatomic, strong) IJKFFMoviePlayerController *player;

@end

@implementation YZLiveViewController
//- (IBAction)back:(id)sender {
//    [self dismissViewControllerAnimated:YES completion:nil];
//}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // 设置直播占位图片
//    NSURL *imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://img.meelive.cn/%@",_live.creator.portrait]];
    NSURL *imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",_live.creator.portrait]];

    [self.imageView sd_setImageWithURL:imageUrl placeholderImage:nil];
    
    // 拉流地址
    NSURL *url = [NSURL URLWithString:_live.stream_addr];
    
    // 创建IJKFFMoviePlayerController：专门用来直播，传入拉流地址就好了
    IJKFFMoviePlayerController *playerVc = [[IJKFFMoviePlayerController alloc] initWithContentURL:url withOptions:nil];
    playerVc.view.frame = [UIScreen mainScreen].bounds;

    // 准备播放
    [playerVc prepareToPlay];

    // 强引用，反正被销毁
    _player = playerVc;

    [self.view insertSubview:playerVc.view atIndex:1];
    
    
//   self.player =  [self.player initWithContentURL:url withOptions:nil];
//    [self.player prepareToPlay];
//    [self.view addSubview:self.player.view];

}

-(IJKFFMoviePlayerController *)player
{
    if (!_player) {
        _player = [[IJKFFMoviePlayerController alloc] initWithContentURL:nil withOptions:nil];
        _player.view.frame = [UIScreen mainScreen].bounds;
    }
    return _player;
}



-(UIImageView *)imageView
{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    }
    return _imageView;
}



- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // 界面消失，一定要记得停止播放
    [_player pause];
    [_player stop];
    [_player shutdown];
}


@end
