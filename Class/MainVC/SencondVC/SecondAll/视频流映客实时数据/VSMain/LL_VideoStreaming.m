//
//  LL_VideoStreaming.m
//  XJW
//
//  Created by John on 2020/1/16.
//  Copyright © 2020 Liam. All rights reserved.
//

#import "LL_VideoStreaming.h"

static LL_VideoStreaming *manager = nil;

@interface LL_VideoStreaming ()

@end

@implementation LL_VideoStreaming

+ (instancetype)sharedManager {
   
    static dispatch_once_t pred;
    dispatch_once(&pred,^{
        manager = [[self alloc]init];
    });
    return manager;
}

-(IJKFFMoviePlayerController *)player
{
    if (!_player) {
        _player = [[IJKFFMoviePlayerController alloc] initWithContentURL:nil withOptions:nil];
        _player.view.frame = [UIScreen mainScreen].bounds;
    }
    return _player;
}

@end
