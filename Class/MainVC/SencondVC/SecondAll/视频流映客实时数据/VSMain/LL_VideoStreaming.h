//
//  LL_VideoStreaming.h
//  XJW
//
//  Created by John on 2020/1/16.
//  Copyright © 2020 Liam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IJKMediaFramework/IJKMediaFramework.h>

NS_ASSUME_NONNULL_BEGIN

@interface LL_VideoStreaming : NSObject

+ (instancetype)sharedManager;

@property(nonatomic , strong) IJKFFMoviePlayerController *player;

@end

NS_ASSUME_NONNULL_END
