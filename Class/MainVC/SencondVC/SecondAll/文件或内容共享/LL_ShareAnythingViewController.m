//
//  LL_ShareAnythingViewController.m
//  XJW
//
//  Created by John on 2019/7/29.
//  Copyright © 2019 Liam. All rights reserved.
//

#import "LL_ShareAnythingViewController.h"

#import "customActivity.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import <SafariServices/SafariServices.h>
#import <Twitter/Twitter.h>

#import "ShareItem.h"

@interface LL_ShareAnythingViewController ()

@end

@implementation LL_ShareAnythingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];

    
    [self showMainView];
}

-(void)showMainView
{
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(20, kScreenHeight/2 -80, kScreenWidth - 40, 50);
    [btn1 setTitle:@"分享到WSL" forState:UIControlStateNormal];
    [btn1 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [self.view addSubview: btn1];
    [btn1 addTarget:self action:@selector(shareToWSL:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.frame = CGRectMake(20, kScreenHeight/2 , kScreenWidth - 40, 50);
    [btn2 setTitle:@"系统分享" forState:UIControlStateNormal];
    [btn2 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [self.view addSubview: btn2];
    [btn2 addTarget:self action:@selector(shareBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn3.frame = CGRectMake(20, kScreenHeight/2 + 80, kScreenWidth - 40, 50);
    [btn3 setTitle:@"微信多图分享" forState:UIControlStateNormal];
    [btn3 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [self.view addSubview: btn3];
    [btn3 addTarget:self action:@selector(addActivityViewController) forControlEvents:UIControlEventTouchUpInside];
}



- (void)shareToWSL:(id)sender {
    
    //不带参数
    NSString * wslUrlScheme = @"WSLAPP://";
    //如果参数含有特殊字符或汉字，需要转码，否则这个URL不合法，就会唤起失败；参数字符串的格式可以自定义，只要便于自己到时候解析就行；
    NSString * parameterStr = [@"wsl，你是猴子请来的救兵吗？" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //不带参数
    //    NSURL * url = [NSURL URLWithString:wslScheme];
    //带参数
    //WSLAPP://name=wsl&weight=保密
    NSURL * url = [NSURL URLWithString:[wslUrlScheme stringByAppendingString:parameterStr]];
    
    //iOS 10以下
    //    [[UIApplication sharedApplication] openURL:url];
    //iOS 10以上
    [[UIApplication sharedApplication] openURL:url options:nil completionHandler:^(BOOL success) {}];
    
}

- (void)shareBtnClicked:(id)sender {
    
    //要分享的内容，加在一个数组里边，初始化UIActivityViewController
    NSString *textToShare = @"我是M.Z_iOS，欢迎关注我！";
//    UIImage *imageToShare = [UIImage imageNamed:@"wang.png"];
    NSURL *urlToShare = [NSURL URLWithString:@"https://blog.csdn.net/sun2728"];
    NSArray *activityItems = @[urlToShare,textToShare];
    
    //自定义Activity
    customActivity * customActivit = [[customActivity alloc] initWithTitie:@"M.Z_iOS" withActivityImage:nil withUrl:urlToShare withType:@"customActivity" withShareContext:activityItems];
    NSArray *activities = @[customActivit];
    
    /**
     创建分享视图控制器
     
     ActivityItems  在执行activity中用到的数据对象数组。数组中的对象类型是可变的，并依赖于应用程序管理的数据。例如，数据可能是由一个或者多个字符串/图像对象，代表了当前选中的内容。
     
     Activities  是一个UIActivity对象的数组，代表了应用程序支持的自定义服务。这个参数可以是nil。
     
     */
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:activities];
    
    //UIActivityViewControllerCompletionWithItemsHandler)(NSString * __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError)  iOS >=8.0
    
    //UIActivityViewControllerCompletionHandler (NSString * __nullable activityType, BOOL completed); iOS 6.0~8.0
    
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        //初始化回调方法
        UIActivityViewControllerCompletionWithItemsHandler myBlock = ^(NSString *activityType,BOOL completed,NSArray *returnedItems,NSError *activityError)
        {
            NSLog(@"activityType :%@", activityType);
            if (completed)
            {
                NSLog(@"completed");
            }
            else
            {
                NSLog(@"cancel");
            }
            
        };
        
        // 初始化completionHandler，当post结束之后（无论是done还是cancell）该blog都会被调用
        activityVC.completionWithItemsHandler = myBlock;
    }else{
        
        UIActivityViewControllerCompletionHandler myBlock = ^(NSString *activityType,BOOL completed)
        {
            NSLog(@"activityType :%@", activityType);
            if (completed)
            {
                NSLog(@"completed");
            }
            else
            {
                NSLog(@"cancel");
            }
            
        };
        // 初始化completionHandler，当post结束之后（无论是done还是cancell）该blog都会被调用
        activityVC.completionHandler = myBlock;
    }
    
    //Activity 类型又分为“操作”和“分享”两大类
    /*
     UIKIT_EXTERN NSString *const UIActivityTypePostToFacebook     NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypePostToTwitter      NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypePostToWeibo        NS_AVAILABLE_IOS(6_0);    //SinaWeibo
     UIKIT_EXTERN NSString *const UIActivityTypeMessage            NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypeMail               NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypePrint              NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypeCopyToPasteboard   NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypeAssignToContact    NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypeSaveToCameraRoll   NS_AVAILABLE_IOS(6_0);
     UIKIT_EXTERN NSString *const UIActivityTypeAddToReadingList   NS_AVAILABLE_IOS(7_0);
     UIKIT_EXTERN NSString *const UIActivityTypePostToFlickr       NS_AVAILABLE_IOS(7_0);
     UIKIT_EXTERN NSString *const UIActivityTypePostToVimeo        NS_AVAILABLE_IOS(7_0);
     UIKIT_EXTERN NSString *const UIActivityTypePostToTencentWeibo NS_AVAILABLE_IOS(7_0);
     UIKIT_EXTERN NSString *const UIActivityTypeAirDrop            NS_AVAILABLE_IOS(7_0);
     */
    
    // 分享功能(Facebook, Twitter, 新浪微博, 腾讯微博...)需要你在手机上设置中心绑定了登录账户, 才能正常显示。
    //关闭系统的一些activity类型
    activityVC.excludedActivityTypes = @[];
    
    //在展现view controller时，必须根据当前的设备类型，使用适当的方法。在iPad上，必须通过popover来展现view controller。在iPhone和iPodtouch上，必须以模态的方式展现。
    [self presentViewController:activityVC animated:YES completion:nil];
    
}


- (void)elseAPI{
    
    //复制链接功能
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = @"需要复制的内容";
    
    //用safari打开网址
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://blog.csdn.net/sun2728"]];
    
    //保存图片到相册
    UIImage *image = [UIImage imageNamed:@"wang"];
    id completionTarget = self;
    SEL completionSelector = @selector(didWriteToSavedPhotosAlbum);
    void *contextInfo = NULL;
    UIImageWriteToSavedPhotosAlbum(image, completionTarget, completionSelector, contextInfo);
    
    
    //添加书签
    NSURL *URL = [NSURL URLWithString:@"https://blog.csdn.net/sun2728"];
    BOOL result = [[SSReadingList defaultReadingList] addReadingListItemWithURL:URL
                                                                          title:@"WSL"
                                                                    previewText:@"M.Z_iOS"
                                                                          error:nil];
    if (result) {
        NSLog(@"添加书签成功");
    }
    
    
    //发送短信
    MFMessageComposeViewController *messageComposeViewController = [[MFMessageComposeViewController alloc] init];
    messageComposeViewController.recipients = @[@"M.Z_iOS"];
    //messageComposeViewController.delegate = self;
    messageComposeViewController.body = @"你好，我是M.Z_iOS，请多指教！";
    messageComposeViewController.subject = @"M.Z_iOS";
    
    
    
    //发送邮件
    MFMailComposeViewController *mailComposeViewController = [[MFMailComposeViewController alloc] init];
    [mailComposeViewController addAttachmentData:nil mimeType:nil fileName:nil];
    [mailComposeViewController setToRecipients:@[@"mattt@nshipster•com"]];
    [mailComposeViewController setSubject:@"WSL"];
    [mailComposeViewController setMessageBody:@"Lorem ipsum dolor sit amet"
                                       isHTML:NO];
    if([MFMailComposeViewController  canSendMail]){
        [self presentViewController:mailComposeViewController animated:YES completion:nil];
    };
    
    
    
    //发送推文
    TWTweetComposeViewController *tweetComposeViewController =
    [[TWTweetComposeViewController alloc] init];
    [tweetComposeViewController setInitialText:@"梦想还是要有的,万一实现了呢!-----M.Z_iOS"];
    [tweetComposeViewController addURL:[NSURL URLWithString:@"https://blog.csdn.net/sun2728"]];
    [tweetComposeViewController addImage:[UIImage imageNamed:@"wang"]];
    if ([TWTweetComposeViewController canSendTweet]) {
        [self presentViewController:tweetComposeViewController animated:YES completion:nil];
    }
    
}

//苹果自带的分享界面
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    return;
    
    //[SLComposeViewController isAvailableForServiceType: @"com.tencent.xin.sharetimeline"];微信
    
    
    //1.判断平台是否可用(系统没有集成,用户设置新浪账号)
    if (![SLComposeViewController isAvailableForServiceType:SLServiceTypeSinaWeibo]) {
        NSLog(@"到设置界面去设置自己的新浪账号");
        return;
    }
    
    // 2.创建分享控制器
    SLComposeViewController *composeVc = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeSinaWeibo];
    
    // 2.1.添加分享的文字
    [composeVc setInitialText:@"梦想还是要有的,万一实现了呢!-----M.Z_iOS"];
    
    // 2.2.添加分享的图片
    [composeVc addImage:[UIImage imageNamed:@"wang.png"]];
    
    // 2.3 添加分享的URL
    [composeVc addURL:[NSURL URLWithString:@"https://blog.csdn.net/sun2728"]];
    
    // 3.弹出控制器进行分享
    [self presentViewController:composeVc animated:YES completion:nil];
    
    // 4.设置监听发送结果
    composeVc.completionHandler = ^(SLComposeViewControllerResult reulst) {
        if (reulst == SLComposeViewControllerResultDone) {
            NSLog(@"用户发送成功");
        } else {
            NSLog(@"用户发送失败");
        }
    };
    
}

// 图片转字符串
- (NSString * )imageToString:(UIImage *)image{
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    NSString *imageDataString = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    return imageDataString;
}

// 字符串转图片
- (UIImage *)imageFromString:(NSString *)string{
    NSData *data=[[NSData alloc] initWithBase64EncodedString:string options:NSDataBase64DecodingIgnoreUnknownCharacters];
    UIImage *image = [UIImage imageWithData:data];
    return image;
}

//字典转json格式字符串：
- (NSString*)dictionaryToJson:(NSDictionary *)dic
{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}
//json格式字符串转字典：
- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                         
                                                        options:NSJSONReadingMutableContainers
                         
                                                          error:&err];
    
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}









#pragma mark ------------------------ 微信多图分享 ------------------------


-(void)addActivityViewController
{
    /**
     第一种：分享类型为纯图片
     */
    //    UIImage *imageToShare = [UIImage imageNamed:@"111.jpg"];
    //    UIImage *imageToShare1 = [UIImage imageNamed:@"222.jpg"];
    //    UIImage *imageToShare2= [UIImage imageNamed:@"333.jpg"];
    //    NSArray *itemArr = @[imageToShare,imageToShare1,imageToShare2];
    
    
    /**
     第二种：图片数组为img的本机缓存地址
     */
    //    UIImage *imageToShare = [UIImage imageNamed:@"111.jpg"];
    //    UIImage *imageToShare1 = [UIImage imageNamed:@"222.jpg"];
    //    UIImage *imageToShare2= [UIImage imageNamed:@"333.jpg"];
    //    NSArray *activityItems = @[imageToShare,imageToShare1,imageToShare2];
    //
    //    NSMutableArray *items = [NSMutableArray array];
    //    NSString *docPath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    //
    //    for (int i = 0; i < activityItems.count; i++) {
    //        //图片缓存的地址，自己进行替换
    //        NSString *imagePath = [docPath stringByAppendingString:[NSString stringWithFormat:@"/ShareWX%d.jpg",i]];
    //        //把图片写进缓存，一定要先写入本地，不然会分享出错
    //        [UIImageJPEGRepresentation(activityItems[i], .5) writeToFile:imagePath atomically:YES];
    //        //把缓存图片的地址转成NSUrl格式
    //        NSURL *shareobj = [NSURL fileURLWithPath:imagePath];
    //        //这个部分是自定义ActivitySource
    //        ShareItem *item = [[ShareItem alloc] initWithData:activityItems[i] andFile:shareobj];
    //        //分享的数组
    //        [items addObject:item];
    //    }
    
    /**
     第三种：图片数组为url的本机缓存地址
     url必须是图片的地址，不是网页的地址
     */
    NSArray *activityItems = @[
                               @"http://img3.duitang.com/uploads/item/201604/24/20160424132044_ZzhuX.jpeg",
                               @"http://v1.qzone.cc/avatar/201408/03/23/44/53de58e5da74c247.jpg%21200x200.jpg",
                               @"http://img4.imgtn.bdimg.com/it/u=1483569741,1992390913&fm=214&gp=0.jpg"];
    NSMutableArray *items = [NSMutableArray array];
    NSString *docPath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)[0];
    for (int i = 0; i < activityItems.count; i++) {
        //取出地址
        NSString *URL = [activityItems[i] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //把图片转成NSData类型
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:URL]];
        //写入图片中
        UIImage *imagerang = [UIImage imageWithData:data];
        //图片缓存的地址，自己进行替换
        NSString *imagePath = [docPath stringByAppendingString:[NSString stringWithFormat:@"/ShareWX%d.jpg",i]];
        //把图片写进缓存，一定要先写入本地，不然会分享出错
        [UIImageJPEGRepresentation(imagerang, .5) writeToFile:imagePath atomically:YES];
        //把缓存图片的地址转成NSUrl格式
        NSURL *shareobj = [NSURL fileURLWithPath:imagePath];
        //这个部分是自定义ActivitySource
        ShareItem *item = [[ShareItem alloc] initWithData: imagerang andFile:shareobj];
        //分享的数组
        [items addObject:item];
    }
    
#pragma mark - 分享功能
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
    //去除特定的分享功能
    activityVC.excludedActivityTypes = @[UIActivityTypePostToFacebook,UIActivityTypePostToTwitter, UIActivityTypePostToWeibo,UIActivityTypeMessage,UIActivityTypeMail,UIActivityTypePrint,UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll,UIActivityTypeAddToReadingList,UIActivityTypePostToFlickr,UIActivityTypePostToVimeo,UIActivityTypePostToTencentWeibo,UIActivityTypeAirDrop,UIActivityTypeOpenInIBooks];
    [self presentViewController: activityVC animated:YES completion:nil];
}






@end
