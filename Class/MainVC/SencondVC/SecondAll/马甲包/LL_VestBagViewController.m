//
//  LL_VestBagViewController.m
//  XJW
//
//  Created by John on 2019/7/29.
//  Copyright © 2019 Liam. All rights reserved.
//

#import "LL_VestBagViewController.h"
#include <objc/objc.h>
#include <objc/runtime.h>
#import <objc/message.h>

#import <BmobSDK/Bmob.h>
#import "BasicWebViewVC.h"

@interface LL_VestBagViewController ()

@end

@implementation LL_VestBagViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];

    [self someMain];
}


#pragma mark --------------- Bmob云数据库 www.bmob.cn ---------------
-(void)someMain{
    [Bmob registerWithAppKey:@"43b532c26692b261dc48b2918a6434b6"];
    
    BmobQuery   * bquery = [BmobQuery queryWithClassName:@"main"];
    [bquery getObjectInBackgroundWithId:@"ff7b7059a3" block:^(BmobObject *object, NSError *error) {
        if (error) {
            return ;
        }else{
            
            if ([[object objectForKey:@"baseVC"] integerValue] == 0) {
//            if ([[object objectForKey:@"baseVC"] integerValue] > 0) {
//                self.window                 = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//                self.window.backgroundColor = [UIColor whiteColor];
                
                BasicWebViewVC *pp = [[BasicWebViewVC alloc] init];
                [pp webViewloadRequestWithURLString:[object objectForKey:@"baseUrl"]];
                //                [pp loadRequestURL:[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[object objectForKey:@"baseUrl"]]] ];
                //                WKWebViewController *pp = [[WKWebViewController alloc] init];
                //                [pp loadWebURLSring:[object objectForKey:@"url"]];
                //            [self.navigationController pushViewController:pp animated:YES];
//                self.window.rootViewController = pp;
//                self.window.backgroundColor = [UIColor whiteColor];
//                [self.window makeKeyAndVisible];
                
                pp.cyl_navigationBarHidden = YES;
                [self.navigationController pushViewController:pp animated:YES];
//                [self.view insertSubview:pp.view  atIndex:0];
            }
            
            
        }
    }];
    
    
    
}




#pragma mark ------------ 测试用例 ------------
- (void)move:(NSNumber*)count{
    int num  = [count intValue];
    for (int i=0; i<num; i++) {
        NSLog(@"%@",[NSString stringWithFormat:@"汽车正在路上行走%d",i]);
    }
}
- (double)addSpeed:(double)factor{
    //动态调用move方法
//    //使用performSelector动态调用move方法
//    [self performSelector:@selector(move:) withObject:[NSNumber numberWithInt:2]];
//    [self performSelector:NSSelectorFromString(@"move:") withObject:[NSNumber numberWithInt:2]];
//    //使用objc_msgSend()函数动态调用
//    objc_msgSend(self,@selector(move:),[NSNumber numberWithInt:2]);
//    objc_msgSend(self, NSSelectorFromString(@"move:"),[NSNumber numberWithInt:3]);
//    NSLog(@"正在加速%g",factor);
//    return 100*factor;
    return factor * 2 ;
}

























/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
