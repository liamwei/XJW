//
//  LL_AdManagerViewController.m
//  XJW
//
//  Created by John on 2019/7/29.
//  Copyright © 2019 Liam. All rights reserved.
//

#import "LL_AdManagerViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "LL_RewardedVideoViewController.h"

typedef NS_ENUM(NSUInteger, GameState) {
    kGameStateNotStarted = 0,  ///< Game has not started.
    kGameStatePlaying = 1,     ///< Game is playing.
    kGameStatePaused = 2,      ///< Game is paused.
    kGameStateEnded = 3        ///< Game has ended.
};

static const NSInteger kGameLength = 5;

@interface LL_AdManagerViewController () <GADBannerViewDelegate , GADInterstitialDelegate >

@property(nonatomic, strong) GADBannerView *bannerView;

/// The AdManager interstitial ad.
@property(nonatomic, strong) DFPInterstitial *interstitial;

/// The countdown timer.
@property(nonatomic, strong) NSTimer *timer;

/// The amount of time left in the game.
@property(nonatomic, assign) NSInteger timeLeft;

/// The state of the game.
@property(nonatomic, assign) GameState gameState;

/// The date that the timer was paused.
@property(nonatomic, strong) NSDate *pauseDate;

/// The last fire date before a pause.
@property(nonatomic, strong) NSDate *previousFireDate;

@property(nonatomic, strong)  UILabel *gameText;
@property(nonatomic, strong)  UIButton *playAgainButton;


@end

@implementation LL_AdManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self showMyBannerView];
//   插页广告
    [self showAdManagerInterstitialExample];
    [self pushBtn];
    
}

-(void)pushBtn{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(kScreenWidth/2, kScreenHeight - 100, kScreenWidth/2, 44);
    [btn setTitle:@"视频剪辑" forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor orangeColor];
    [self.view addSubview:btn];
    [btn addTarget:self action:@selector(btnPressed) forControlEvents:UIControlEventTouchUpInside];
}
-(void)btnPressed{
    LL_RewardedVideoViewController *vc = [[LL_RewardedVideoViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma ----------- BannerView

-(void)showMyBannerView
{
//    GADAdSize size = GADAdSizeFromCGSize(CGSizeMake(kScreenWidth, 50));

    self.bannerView = [[GADBannerView alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, 50)];
    [self addBannerViewToView:self.bannerView];
    
    self.bannerView.adUnitID = @"admob1ca-app-pub-3746946879631096/9479243547";
//    self.bannerView.adUnitID = @"/6499/example/banner";
    
    self.bannerView.rootViewController = self;
    self.bannerView.delegate = self;
    [self.bannerView loadRequest:[DFPRequest request]];

}
- (void)addBannerViewToView:(UIView *)bannerView {
    bannerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:bannerView];
    [self.view addConstraints:@[
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.bottomLayoutGuide
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:0],
                                [NSLayoutConstraint constraintWithItem:bannerView
                                                             attribute:NSLayoutAttributeCenterX
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self.view
                                                             attribute:NSLayoutAttributeCenterX
                                                            multiplier:1
                                                              constant:0]
                                ]];
}


// 接收到广告 再 显示广告
- (void)adViewDidReceiveAd:(DFPBannerView *)adView {
    // Add adView to view and add constraints as above.
    NSLog(@"adViewDidReceiveAd");

    [self addBannerViewToView:self.bannerView];
    adView.alpha = 0;
    [UIView animateWithDuration:1.0 animations:^{
        adView.alpha = 1;
    }];
}


/// Tells the delegate an ad request failed.
- (void)adView:(DFPBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", [error localizedDescription]);
//    [self.bannerView loadRequest:[DFPRequest request]];
}

/// Tells the delegate that a full-screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(DFPBannerView *)adView {
    NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full-screen view will be dismissed.
- (void)adViewWillDismissScreen:(DFPBannerView *)adView {
    NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full-screen view has been dismissed.
- (void)adViewDidDismissScreen:(DFPBannerView *)adView {
    NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(DFPBannerView *)adView {
    NSLog(@"adViewWillLeaveApplication");
}



#pragma ----------- AdManagerInterstitialExample -----------

-(void)showAdManagerInterstitialExample{
    self.gameText = [[UILabel alloc] initWithFrame:CGRectMake(20, kScreenHeight/2 - 100, kScreenWidth - 40, 50)];
    self.gameText.textColor = [UIColor blackColor];
    [self.view addSubview:self.gameText];
    
    self.playAgainButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.playAgainButton.frame = CGRectMake(20, kScreenHeight/2, kScreenWidth - 40, 50);
    [self.playAgainButton setTitle:@"Play Again" forState:UIControlStateNormal];
    self.playAgainButton.backgroundColor = [UIColor orangeColor];
    [self.view addSubview:self.playAgainButton];
    [self.playAgainButton addTarget:self action:@selector(playAgain:) forControlEvents:UIControlEventTouchUpInside];

    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                         selector:@selector(pauseGame)
                                             name:UIApplicationDidEnterBackgroundNotification
                                           object:nil];

// Resume game when application becomes active.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                         selector:@selector(resumeGame)
                                             name:UIApplicationDidBecomeActiveNotification
                                           object:nil];

    [self startNewGame];
}

#pragma mark Game logic

- (void)startNewGame {
    [self createAndLoadInterstitial];
    
    self.gameState = kGameStatePlaying;
    self.playAgainButton.hidden = YES;
    self.timeLeft = kGameLength;
    [self updateTimeLeft];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                  target:self
                                                selector:@selector(decrementTimeLeft:)
                                                userInfo:nil
                                                 repeats:YES];
}

- (void)createAndLoadInterstitial {
    
//    self.interstitial = [[DFPInterstitial alloc] initWithAdUnitID:@"/6499/example/interstitial"];
    self.interstitial = [[DFPInterstitial alloc] initWithAdUnitID:@"ca-app-pub-3746946879631096/7610955658"];
    self.interstitial.delegate = self;
    [self.interstitial loadRequest:[DFPRequest request]];
    [self.interstitial presentFromRootViewController:self];
}
/// Called when the banner receives an app event.
- (void)adView:(nonnull GADBannerView *)banner didReceiveAppEvent:(nonnull NSString *)name withInfo:(nullable NSString *)info
{
    NSLog(@"name = %@",name);
}

/// Called when the interstitial receives an app event.
- (void)interstitial:(nonnull GADInterstitial *)interstitial didReceiveAppEvent:(nonnull NSString *)name withInfo:(nullable NSString *)info
{
    NSLog(@"name = %@",name);
}


/// Tells the delegate an ad request succeeded.
- (void)interstitialDidReceiveAd:(DFPInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
}

/// Tells the delegate an ad request failed.
- (void)interstitial:(DFPInterstitial *)ad
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"interstitial:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

/// Tells the delegate that an interstitial will be presented.
- (void)interstitialWillPresentScreen:(DFPInterstitial *)ad {
    NSLog(@"interstitialWillPresentScreen");
}

/// Tells the delegate the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(DFPInterstitial *)ad {
    NSLog(@"interstitial Will DismissScreen");
}

/// Tells the delegate the interstitial had been animated off the screen.
- (void)interstitialDidDismissScreen:(DFPInterstitial *)ad {
    NSLog(@"interstitial Did Dismiss Screen");
}

/// Tells the delegate that a user click will open another app
/// (such as the App Store), backgrounding the current app.
- (void)interstitialWillLeaveApplication:(DFPInterstitial *)ad {
    NSLog(@"interstitial Will LeaveApplication");
}



- (void)updateTimeLeft {
    self.gameText.text = [NSString stringWithFormat:@"%ld seconds left!", (long)self.timeLeft];
}

- (void)decrementTimeLeft:(NSTimer *)timer {
    self.timeLeft--;
    [self updateTimeLeft];
    if (self.timeLeft == 0) {
        [self endGame];
    }
}

- (void)pauseGame {
    if (self.gameState != kGameStatePlaying) {
        return;
    }
    self.gameState = kGameStatePaused;
    
    // Record the relevant pause times.
    self.pauseDate = [NSDate date];
    self.previousFireDate = [self.timer fireDate];
    
    // Prevent the timer from firing while app is in background.
    [self.timer setFireDate:[NSDate distantFuture]];
}

- (void)resumeGame {
    if (self.gameState != kGameStatePaused) {
        return;
    }
    self.gameState = kGameStatePlaying;
    
    // Calculate amount of time the app was paused.
    float pauseTime = [self.pauseDate timeIntervalSinceNow] * -1;
    
    // Set the timer to start firing again.
    [self.timer setFireDate:[NSDate dateWithTimeInterval:pauseTime sinceDate:self.previousFireDate]];
}

- (void)endGame {
    self.gameState = kGameStateEnded;
    [self.timer invalidate];
    self.timer = nil;
    
    [[[UIAlertView alloc]
      initWithTitle:@"Game Over"
      message:[NSString stringWithFormat:@"You lasted %ld seconds", (long)kGameLength]
      delegate:self
      cancelButtonTitle:@"Ok"
      otherButtonTitles:nil] show];
}

- (void)playAgain:(id)sender {
    [self startNewGame];
}

#pragma mark UIAlertViewDelegate implementation

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (self.interstitial.isReady) {
        [self.interstitial presentFromRootViewController:self];
    } else {
        NSLog(@"Ad wasn't ready");
    }
    self.playAgainButton.hidden = NO;
}

#pragma mark dealloc

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidEnterBackgroundNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
}












#pragma mark Watch Video for 10 additional coins  视频得奖

























@end
