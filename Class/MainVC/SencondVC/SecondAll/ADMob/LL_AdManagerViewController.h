//
//  LL_AdManagerViewController.h
//  XJW
//
//  Created by John on 2019/7/29.
//  Copyright © 2019 Liam. All rights reserved.
//

#import <CYLTabBarController/CYLTabBarController.h>

NS_ASSUME_NONNULL_BEGIN

@interface LL_AdManagerViewController : CYLBaseViewController




@end

NS_ASSUME_NONNULL_END





/**
 请按照以下说明操作：
 请使用此应用 ID 完成 Google 移动广告 SDK 指南中的说明：
 计算器-保险箱ca-app-pub-3746946879631096~4689278842
 请按照横幅广告实现指南来集成 SDK。在使用此广告单元 ID 集成代码时，您需要指定广告类型、尺寸和展示位置：
 admob1ca-app-pub-3746946879631096/9479243547
 请查看 AdMob 政策，确保您的实现方案符合相关规定。
 
 
 插页广告
 请按照以下说明操作：
 请使用此应用 ID 完成 Google 移动广告 SDK 指南中的说明：
 计算器-保险箱ca-app-pub-3746946879631096~4689278842
 请按照插页式广告实现指南来集成 SDK。在使用此广告单元 ID 集成代码时，您需要指定广告类型和展示位置：
 插页广告ca-app-pub-3746946879631096/7610955658
 
 
 
 
 激励广告， 看完奖励
 请按照以下说明操作：
 请使用此应用 ID 完成 Google 移动广告 SDK 指南中的说明：
 计算器-保险箱ca-app-pub-3746946879631096~4689278842
 请按照激励广告实现指南来集成 SDK。在使用此广告单元 ID 集成代码时，您需要指定广告类型和展示位置：
 激励广告ca-app-pub-3746946879631096/8191633487
 
 */
