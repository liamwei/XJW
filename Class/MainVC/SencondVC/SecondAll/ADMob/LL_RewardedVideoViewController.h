//
//  LL_RewardedVideoViewController.h
//  XJW
//
//  Created by John on 2019/7/30.
//  Copyright © 2019 Liam. All rights reserved.
//

#import <CYLTabBarController/CYLTabBarController.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

NS_ASSUME_NONNULL_BEGIN

@interface LL_RewardedVideoViewController : CYLBaseViewController <GADRewardBasedVideoAdDelegate>


/// The game text.
@property(strong, nonatomic)  UILabel *gameLabel;

/// The play again button.
@property(strong, nonatomic)  UIButton *playAgainButton;

/// The button to show rewarded video.
@property(strong, nonatomic)  UIButton *showVideoButton;

/// The text indicating current coin count.
@property(strong, nonatomic)  UILabel *coinCountLabel;

/// Restarts the game.
- (void)playAgain:(id)sender;

/// Shows a rewarded video.
- (void)showVideo:(id)sender;

/// Pauses the game.
- (void)pauseGame;

/// Resumes the game.
- (void)resumeGame;



@end

NS_ASSUME_NONNULL_END
