//
//  LL_RewardedVideoViewController.m
//  XJW
//
//  Created by John on 2019/7/30.
//  Copyright © 2019 Liam. All rights reserved.
//

#import "LL_RewardedVideoViewController.h"

/// Constant for coin rewards.
static const NSInteger GameOverReward = 1;

/// Starting time for game counter.
static const NSInteger GameLength = 4;

typedef NS_ENUM(NSInteger, GameState) {
    kGameStateNotStarted = 0,  ///< Game has not started.
    kGameStatePlaying = 1,     ///< Game is playing.
    kGameStatePaused = 2,      ///< Game is paused.
    kGameStateEnded = 3        ///< Game has ended.
};

@interface LL_RewardedVideoViewController ()

/// Number of coins the user has earned.
@property(nonatomic, assign) NSInteger coinCount;

/// The countdown timer.
@property(nonatomic, strong) NSTimer *timer;

/// The game counter.
@property(nonatomic, assign) NSInteger counter;

/// The state of the game.
@property(nonatomic, assign) GameState gameState;

/// The date that the timer was paused.
@property(nonatomic, strong) NSDate *pauseDate;

/// The last fire date before a pause.
@property(nonatomic, strong) NSDate *previousFireDate;

@end

@implementation LL_RewardedVideoViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.


    [GADRewardBasedVideoAd sharedInstance].delegate = self;
    self.coinCount = 0;
    [self initMainView];
    [self startNewGame];
}



-(void)initMainView
{
    self.gameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 50, kScreenWidth -40, 50)];
    self.gameLabel.text = @" Game Started";
    self.gameLabel.textAlignment = NSTextAlignmentCenter;
    self.gameLabel.textColor = [UIColor blackColor];
    [self.view addSubview:self.gameLabel];
    
    self.playAgainButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.playAgainButton.frame = CGRectMake(20, kScreenHeight/2 - 70, kScreenWidth -40, 50);
    self.playAgainButton.backgroundColor = [UIColor orangeColor];
    [self.playAgainButton setTitle:@"Play Again" forState:UIControlStateNormal];
    [self.view addSubview:self.playAgainButton];
    [self.playAgainButton addTarget:self action:@selector(playAgain:) forControlEvents:UIControlEventTouchUpInside];
    
    self.showVideoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.showVideoButton.frame = CGRectMake(20, kScreenHeight/2, kScreenWidth -40, 50);
    self.showVideoButton.backgroundColor = [UIColor orangeColor];
    [self.showVideoButton setTitle:@"Watch Video for 10 additional coins" forState:UIControlStateNormal];
    [self.view addSubview:self.showVideoButton];
    [self.showVideoButton addTarget:self action:@selector(showVideo:) forControlEvents:UIControlEventTouchUpInside];


    self.coinCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, kScreenHeight/2 +50, kScreenWidth -40, 50)];
    self.coinCountLabel.text = @"Coins : 0";
    self.coinCountLabel.textColor = [UIColor blackColor];
    [self.view addSubview:self.coinCountLabel];
}






- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self resumeGame];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self pauseGame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Game logic

- (void)startNewGame {
    if (![[GADRewardBasedVideoAd sharedInstance] isReady]) {
        [self requestRewardedVideo];
    }
    self.gameState = kGameStatePlaying;
    self.playAgainButton.hidden = YES;
    self.showVideoButton.hidden = YES;
    self.counter = GameLength;
    self.gameLabel.text = [NSString stringWithFormat:@"%ld", (long)self.counter];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(decrementCounter:)
                                                userInfo:nil
                                                 repeats:YES];
    self.timer.tolerance = GameLength * 0.1;
}

- (void)requestRewardedVideo {
    DFPRequest *request = [DFPRequest request];
    
//    [[GADRewardBasedVideoAd sharedInstance] loadRequest:request withAdUnitID:@"/6499/example/rewarded-video"];
    [[GADRewardBasedVideoAd sharedInstance] loadRequest:request withAdUnitID:@"ca-app-pub-3746946879631096/8191633487"];

}

- (void)pauseGame {
    if (self.gameState != kGameStatePlaying) {
        return;
    }
    self.gameState = kGameStatePaused;
    
    // Record the relevant pause times.
    self.pauseDate = [NSDate date];
    self.previousFireDate = [self.timer fireDate];
    
    // Prevent the timer from firing while app is in background.
    [self.timer setFireDate:[NSDate distantFuture]];
}

- (void)resumeGame {
    if (self.gameState != kGameStatePaused) {
        return;
    }
    self.gameState = kGameStatePlaying;
    
    // Calculate amount of time the app was paused.
    NSTimeInterval pauseDuration = [self.pauseDate timeIntervalSinceNow];
    
    // Set the timer to start firing again.
    [self.timer setFireDate:[self.previousFireDate dateByAddingTimeInterval:-pauseDuration]];
}

- (void)setTimer:(NSTimer *)timer {
    [_timer invalidate];
    _timer = timer;
}

- (void)decrementCounter:(NSTimer *)timer {
    self.counter--;
    if (self.counter > 0) {
        self.gameLabel.text = [NSString stringWithFormat:@"%ld", (long)self.counter];
    } else {
        [self endGame];
    }
}

- (void)earnCoins:(NSInteger)coins {
    self.coinCount += coins;
    [self.coinCountLabel setText:[NSString stringWithFormat:@"Coins: %ld", (long)self.coinCount]];
}

- (void)endGame {
    self.timer = nil;
    self.gameState = kGameStateEnded;
    self.gameLabel.text = @"Game over!";
    if ([[GADRewardBasedVideoAd sharedInstance] isReady]) {
        self.showVideoButton.hidden = NO;
    }
    self.playAgainButton.hidden = NO;
    // Reward user with coins for finishing the game.
    [self earnCoins:GameOverReward];
}

#pragma Interstitial button actions

- (IBAction)playAgain:(id)sender {
    [self startNewGame];
}

- (IBAction)showVideo:(id)sender {
    if ([[GADRewardBasedVideoAd sharedInstance] isReady]) {
        [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:self];
    } else {
        [[[UIAlertView alloc]
          initWithTitle:@"Interstitial not ready"
          message:@"The interstitial didn't finish " @"loading or failed to load"
          delegate:self
          cancelButtonTitle:@"Drat"
          otherButtonTitles:nil] show];
    }
}

#pragma mark UIAlertViewDelegate implementation

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    [self startNewGame];
}

#pragma mark GADRewardBasedVideoAdDelegate implementation

- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is received.");
}

- (void)rewardBasedVideoAdDidOpen:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Opened reward based video ad.");
}

- (void)rewardBasedVideoAdDidStartPlaying:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad started playing.");
}

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is closed.");
    self.showVideoButton.hidden = YES;
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
   didRewardUserWithReward:(GADAdReward *)reward {
    NSString *rewardMessage =
    [NSString stringWithFormat:@"Reward received with currency %@ , amount %lf", reward.type,
     [reward.amount doubleValue]];
    NSLog(@"%@", rewardMessage);
    // Reward the user for watching the video.
    [self earnCoins:[reward.amount integerValue]];
    self.showVideoButton.hidden = YES;
}

- (void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad will leave application.");
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error {
    NSLog(@"Reward based video ad failed to load.");
}

    
    
    
    
    

@end
