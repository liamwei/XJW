//
//  LL_ PhotoTweaksViewController.m
//  XJW
//
//  Created by John on 2019/12/27.
//  Copyright © 2019 Liam. All rights reserved.
//

#import "LL_PhotoTweaksViewController.h"
#import "PhotoTweaksViewController.h"
#import <Photos/Photos.h>

@interface LL_PhotoTweaksViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate, PhotoTweaksViewControllerDelegate>

@property (nonatomic , strong) UIImagePickerController *imagePicker;
@end

@implementation LL_PhotoTweaksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.isLibaryAuthStatusCorrect) {
         [self presentViewController:self.imagePicker animated:YES completion:^{}];
    }
    
 
}

- (BOOL)isLibaryAuthStatusCorrect {
    PHAuthorizationStatus authStatus = [PHPhotoLibrary authorizationStatus];
    if (authStatus == PHAuthorizationStatusNotDetermined || authStatus == PHAuthorizationStatusAuthorized) {
        return YES;
    }
    return NO;
}


-(void)viewDidAppear:(BOOL)animated
{
//    [super viewDidAppear:YES];
//    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    picker.delegate = self;
//    picker.allowsEditing = NO;
//    picker.navigationBarHidden = YES;
//    [self.navigationController presentViewController:picker animated:YES completion:^{
//
//    }];
  
   
}

- (UIImagePickerController *)imagePicker {
    if (!_imagePicker) {
        _imagePicker = [[UIImagePickerController alloc]init];
        
        _imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;
        _imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [[UIBarButtonItem appearance] setTintColor:[UIColor blackColor]];
        NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
        attrs[NSForegroundColorAttributeName] = [UIColor blackColor];
        [[UINavigationBar appearance] setTitleTextAttributes:attrs];
        _imagePicker.delegate = self;
    }
    return _imagePicker;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    PhotoTweaksViewController *photoTweaksViewController = [[PhotoTweaksViewController alloc] initWithImage:image];
    photoTweaksViewController.delegate = self;
    photoTweaksViewController.autoSaveToLibray = YES;
    photoTweaksViewController.maxRotationAngle = M_PI_4;
    [picker pushViewController:photoTweaksViewController animated:YES];
}

- (void)photoTweaksController:(PhotoTweaksViewController *)controller didFinishWithCroppedImage:(UIImage *)croppedImage
{
    [controller.navigationController popViewControllerAnimated:YES];
}

- (void)photoTweaksControllerDidCancel:(PhotoTweaksViewController *)controller
{
    [controller.navigationController popViewControllerAnimated:YES];
}




@end
