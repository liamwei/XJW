//
//  MeiziCell.m
//  Meizi
//
//  Created by Sunnyyoung on 15/7/14.
//  Copyright (c) 2015年 Sunnyyoung. All rights reserved.
//

#import "MeiziCell.h"
#import "Meizi.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface MeiziCell ()

@end

@implementation MeiziCell



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//          self.imageView = [[UIImageView alloc] initWithFrame:self.frame];

        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(1, 1, self.frame.size.width-1, self.frame.size.height - 1)];
//        self.imageView.layer.cornerRadius = 6.0;
//        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
//        self.imageView.layer.masksToBounds = YES;
        [self.contentView addSubview:self.imageView];

    }
      return self;
}
#pragma mark - Public method

- (void)setMeizi:(Meizi *)meizi {
//    NSURL *imageURL = [NSURL URLWithString:meizi.thumb_url];
    NSURL *imageURL = [NSURL URLWithString:meizi.image_url];

    [self.imageView setImageWithURL:imageURL usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
}

@end
