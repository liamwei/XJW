//
//  LL_SencondViewController.m
//  XJW
//
//  Created by John on 2019/7/27.
//  Copyright © 2019 Liam. All rights reserved.
//

#import "LL_SencondViewController.h"
//#import "LL_MainTabbarViewController.H"
//#import "LL_MainRootViewController.h"
//#import <MJRefresh/MJRefresh.h>
#import "LL_DetailsViewController.h"
#import <MJRefresh/MJRefresh.h>
#include <objc/objc.h>
#include <objc/runtime.h>
#import <objc/message.h>
#import "LL_AdManagerViewController.h"


@interface LL_SencondViewController ()

@property (strong ,nonatomic) NSArray *nameArray;
@property (strong ,nonatomic) NSArray *vcArray;


@end

@implementation LL_SencondViewController

-(void)initData{
    
    self.nameArray = [NSArray arrayWithObjects:
                      @"追书神器 小说阅读器",
                      @"大图裁减成圆图",
                      @"映客实时数据 Video streaming",
                      @"自动高度图片 UITableview",
                      @"网站妹子图 dbmeinv.com",
                      @"iOS10网络权限和其他权限申请",
                      @"图片裁减库PhotoTweaks",
                      @"各种视频 抖音 等",
                      @"KTV 展示",
                      @"马甲包包装",
                      @"bannerView",
                      @"文件或内容分享给他人",
                      nil];
    
    self.vcArray   = [NSArray arrayWithObjects:
                      @"XXMainViewController",
                      @"RSKExampleViewController",
                      @"YZMainViewController",
                      @"LL_MeiziPicturesViewController",
                      @"MeiziViewController",
                      @"LL_AllKindsAuthorityViewController",
                      @"LL_PhotoTweaksViewController",
                      @"ZFViewController",
                      @"LL_KTVHTTPCacheVC",
                      @"LL_VestBagViewController",
                      @"LL_AdManagerViewController",
                      @"LL_ShareAnythingViewController",
                      nil];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"同城";    //✅sets navigation bar title.The right way to set the title of the navigation
    self.tabBarItem.title = @"同城";   //❌sets tab bar title. Even the `tabBarItem.title` changed, this will be ignored in tabbar.
    //self.title = @"同城1";                //❌sets both of these. Do not do this‼️‼️ This may cause something strange like this : http://i68.tinypic.com/282l3x4.jpg .
    __weak __typeof(self) weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //Call this Block When enter the refresh status automatically
        NSUInteger delaySeconds = 1;
        dispatch_time_t when = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delaySeconds * NSEC_PER_SEC));
        dispatch_after(when, dispatch_get_main_queue(), ^{
            [weakSelf.tableView.mj_header endRefreshing];
        });
    }];
    
    self.tableView.tableFooterView = [UIView new];
}




- (void)refresh {
    [self.tableView.mj_header beginRefreshing];
    NSUInteger delaySeconds = 1;
    dispatch_time_t when = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delaySeconds * NSEC_PER_SEC));
    dispatch_after(when, dispatch_get_main_queue(), ^{
        [self.tableView.mj_header endRefreshing];
    });
}
#pragma mark - Methods

- (void)configureCell:(UITableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath {
    [[cell textLabel] setText:[NSString stringWithFormat:@" 🌈👉 🌜 %@ 🌛 👈 🌹 ", [self.nameArray objectAtIndex:indexPath.row]]];
}

#pragma mark - Table view

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [self configureCell:cell forIndexPath:indexPath];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.vcArray count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    UIViewController *viewController = [[LL_DetailsViewController alloc] init];
//    UIViewController *viewController = [[LL_AdManagerViewController alloc] init];
    
    NSString *string = [self.vcArray objectAtIndex:indexPath.row];
    Class myClass = NSClassFromString(string);
    id newClass = [[myClass alloc] init];
    
//    if ([string isEqualToString:@"LL_VestBagViewController"]) {
//        [newClass performSelector:@selector(addSpeed:) withObject:[NSNumber numberWithLong:2]];
//        //使用Objec_send来动态调用
//        objc_send(newClass,@selector(addSpeed:),3.4);
//        //定义函数指针变量
//        double (*addSpeed)(id ,SEL,double);
//        //获取car对象的addSpeed方法，再把addSpeed方法赋值给addSpeed指针变量
//        addSpeed = (double (*)(id,SEL,double))[newClass methodForSelector:NSSelectorFromString(@"addSpeed:")];
//        //调用addSpeed
//        double speed = addSpeed(newClass, @selector(addSpeed:), 3.4);
//    }
    
    
    
    //    viewController.hidesBottomBarWhenPushed = YES;  // This property needs to be set before pushing viewController to the navigationController's stack. Meanwhile as it is all base on CYLBaseNavigationController, there is no need to do this.
    
    [newClass cyl_setNavigationBarHidden:YES];
    [self.navigationController pushViewController:newClass animated:YES];
}


@end
