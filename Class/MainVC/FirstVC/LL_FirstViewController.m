//
//  LL_FirstViewController.m
//  XJW
//
//  Created by John on 2019/7/27.
//  Copyright © 2019 Liam. All rights reserved.
//

#import "LL_FirstViewController.h"
#import "LL_MainTabbarViewController.H"
#import "LL_MainRootViewController.h"
#import <MJRefresh/MJRefresh.h>

#import "HomeVideoCollectionViewCell.h"
#import "WMPlayerModel.h"
#import "DetailViewController.h"
#import "VideoDataModel.h"

@interface LL_FirstViewController ()<UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource>

@property (nonatomic, retain) UICollectionView *videoCollectionView;//热拍
@property (nonatomic, strong) NSMutableArray* videoDataAry;

@end

@implementation LL_FirstViewController
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"视频推荐";
    UICollectionViewFlowLayout* videoFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    [videoFlowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];//垂直滚动
    self.videoCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height) collectionViewLayout:videoFlowLayout];
    self.videoCollectionView.alwaysBounceVertical = YES;//当不够一屏的话也能滑动
    self.videoCollectionView.delegate = self;
    self.videoCollectionView.dataSource = self;
    [self.videoCollectionView setBackgroundColor:[UIColor whiteColor]];
    [self.videoCollectionView registerClass:[HomeVideoCollectionViewCell class] forCellWithReuseIdentifier:@"HomeVideoCollectionViewCell"];
    [self.view addSubview:self.videoCollectionView];
    __weakSelf__

//    self.videoCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshVideoData)];
//    self.videoCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(addVideoData)];
    
    self.videoCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
          [weakSelf refreshVideoData];
      }];
    self.videoCollectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf addVideoData];
    }];
    
    
    [VideoDataModel getHomePageVideoDataWithBlock:^(NSArray *dateAry, NSError *error) {
        if (!error) {
            weakSelf.videoDataAry = [NSMutableArray arrayWithArray:dateAry];
            [self.videoCollectionView reloadData];
        }else{
            
        }
    }];
}
-(void)refreshVideoData{
    [VideoDataModel getHomePageVideoDataWithBlock:^(NSArray *dateAry, NSError *error) {
        if (!error) {
            self->_videoDataAry = [NSMutableArray arrayWithArray:dateAry];
            [self.videoCollectionView.mj_header endRefreshing];
            [self.videoCollectionView reloadData];
        }else{
            [self.videoCollectionView.mj_header endRefreshing];
        }
    }];
}
-(void)addVideoData{
    [VideoDataModel getHomePageVideoDataWithBlock:^(NSArray *dateAry, NSError *error) {
        if (!error) {
            //            _videoDataAry = [NSMutableArray arrayWithArray:dateAry];
            [self->_videoDataAry addObjectsFromArray:dateAry];
            [self.videoCollectionView.mj_footer endRefreshing];
            [self.videoCollectionView reloadData];
        }else{
            [self.videoCollectionView.mj_footer endRefreshing];
        }
    }];
}

#pragma mark UICollectionViewDataSource {
//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
//定义展示的UICollectionViewCell的个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _videoDataAry.count;
}
//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HomeVideoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeVideoCollectionViewCell" forIndexPath:indexPath];
    cell.DataModel = self.videoDataAry[indexPath.row];
    return cell;
}
//返回头headerView的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeZero;
}
//定义每个item的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(([UIScreen mainScreen].bounds.size.width)/2 - 2, (([UIScreen mainScreen].bounds.size.width)/2 - 2)*1.6);
}
//定义每个Section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0.0,2.0,0.0,2.0);
}
//每个item之间的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0.0f;
}
//每个section中不同的行之间的行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0.0f;
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    VideoDataModel *videoModel = self.videoDataAry[indexPath.row];
    WMPlayerModel *playerModel = [WMPlayerModel new];
    playerModel.videoURL = [NSURL URLWithString:videoModel.video_url];
//    playerModel.videoURL = [NSURL URLWithString:@"http://static.tripbe.com/videofiles/20121214/9533522808.f4v.mp4"];
//    playerModel.videoURL = [NSURL URLWithString:@"https://xiamivideo.b-cdn.net/file/xiamivideo/m3u8/jav101avid5e0d6a4c8be03/index.m3u8"];
    playerModel.title = videoModel.nickname;
    DetailViewController *detailVC = [DetailViewController new];
           detailVC.playerModel = playerModel;
    [self.navigationController pushViewController:detailVC animated:YES];
}

-(void)dealloc{
    NSLog(@"%@ dealloc",[self class]);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}




//- (void)viewDidLoad {
//    [super viewDidLoad];
//    self.view.backgroundColor = [UIColor whiteColor];
//    self.navigationItem.title = @"首页(3)"; //✅sets navigation bar title.The right way to set the title of the navigation
//    self.tabBarItem.title = @"首页";   //❌sets tab bar title. Even the `tabBarItem.title` changed, this will be ignored in tabbar.
//    //self.title = @"首页1";                //❌sets both of these. Do not do this‼️‼️This may cause something strange like this : http://i68.tinypic.com/282l3x4.jpg .
//    //    [self.navigationController.tabBarItem setBadgeValue:@"3"];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"refresh TabBar" style:UIBarButtonItemStylePlain target:self action:@selector(refreshTabBar:)];
//    __weak __typeof(self) weakSelf = self;
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        //Call this Block When enter the refresh status automatically
//        NSUInteger delaySeconds = 1;
//        dispatch_time_t when = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delaySeconds * NSEC_PER_SEC));
//        dispatch_after(when, dispatch_get_main_queue(), ^{
//            [weakSelf.tableView.mj_header endRefreshing];
//        });
//    }];
//    self.tableView.tableFooterView = [UIView new];
//
//}
//
//- (void)refresh {
//    [self.tableView.mj_header beginRefreshing];
//    NSUInteger delaySeconds = 1;
//    dispatch_time_t when = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delaySeconds * NSEC_PER_SEC));
//    dispatch_after(when, dispatch_get_main_queue(), ^{
//        [self.tableView.mj_header endRefreshing];
//    });
//}
//
//- (void)refreshTabBar:(id)sender {
//    [self createNewTabBardynamically];
//}
//
//- (void)createNewTabBardynamically {
//    id<UIApplicationDelegate> delegate = ((id<UIApplicationDelegate>)[[UIApplication sharedApplication] delegate]);
//    UIWindow *window = delegate.window;
//    LL_MainRootViewController *rootController = (LL_MainRootViewController *)window.rootViewController;
//    [rootController createNewTabBarWithContext:NSStringFromClass([self class])];
//}
//
//#pragma mark - Methods
//
//- (void)configureCell:(UITableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath {
//    [[cell textLabel] setText:[NSString stringWithFormat:@"%@ CYLTabBarController %@", self.tabBarItem.title, @(indexPath.row)]];
//}
//
//#pragma mark - Table view
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    static NSString * CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (!cell) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//    }
//    [self configureCell:cell forIndexPath:indexPath];
//    return cell;
//}
//
//#pragma mark - UINavigationControllerDelegate
//
//- (void)navigationController:(UINavigationController *)navigationController
//       didShowViewController:(UIViewController *)viewController
//                    animated:(BOOL)animate {
//    if ([navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
//        if ([navigationController.viewControllers count] == 1) {
//            navigationController.interactivePopGestureRecognizer.enabled = NO;
//        } else {
//            navigationController.interactivePopGestureRecognizer.enabled = YES;
//        }
//    }
//}
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return 30;
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    NSNumber *badgeNumber = @(indexPath.row);
//    self.navigationItem.title = [NSString stringWithFormat:@"首页(%@)", badgeNumber]; //sets navigation bar title.
//
//    //    [self.navigationController.tabBarItem setBadgeValue:[NSString stringWithFormat:@"%@", badgeNumber]];
//
//    //    CYLTabBarControllerConfig *tabBarControllerConfig = [[CYLTabBarControllerConfig alloc] init];
//    //    CYLTabBarController *tabBarController = tabBarControllerConfig.tabBarController;
//    //    tabBarController.delegate = self;
//    //
//    [self cyl_showBadgeValue:[NSString stringWithFormat:@"%@", @(indexPath.row)] animationType:CYLBadgeAnimationTypeScale];
//    [self pushToNewViewController];
//}
//
//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    [self pushToNewViewController];
//}
//
//- (void)pushToNewViewController {
//    CYLBaseViewController *viewController = [CYLBaseViewController new];
//    viewController.view.backgroundColor = [UIColor orangeColor];
//    [viewController cyl_setNavigationBarHidden:YES];
//    [self.navigationController  pushViewController:viewController animated:YES];
//}
//




@end
