//
//  LL_FirstViewController.h
//  XJW
//
//  Created by John on 2019/7/27.
//  Copyright © 2019 Liam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

#import <CYLTabBarController/CYLTabBarController.h>

NS_ASSUME_NONNULL_BEGIN

//@interface LL_FirstViewController : CYLBaseTableViewController <UITabBarControllerDelegate>

@interface LL_FirstViewController : BaseViewController <UITabBarControllerDelegate>

- (void)refresh;

@end

NS_ASSUME_NONNULL_END
