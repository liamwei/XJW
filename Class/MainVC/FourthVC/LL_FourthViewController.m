//
//  LL_FourthViewController.m
//  XJW
//
//  Created by John on 2019/7/27.
//  Copyright © 2019 Liam. All rights reserved.
//  我的界面

#import "LL_FourthViewController.h"
//#import "LL_MainTabbarViewController.H"
//#import "LL_MainRootViewController.h"
//#import <MJRefresh/MJRefresh.h>
#import "LL_DetailsViewController.h"
#import <MJRefresh/MJRefresh.h>


@interface LL_FourthViewController ()

@property (strong ,nonatomic) NSArray *nameArray;
@property (strong ,nonatomic) NSArray *vcArray;

@end

@implementation LL_FourthViewController




-(void)initData{
    
    self.nameArray = [NSArray arrayWithObjects:
                      @"建七彩文件夹",
                      @"PDF阅读器，GIF切割",
                      @"本地常用功能封装",@"网页和本地文件",@"多Ping 和 单Ping",
                      nil];
    
    self.vcArray   = [NSArray arrayWithObjects:
                      @"PNoteMainViewController",
                      @"LL_ReaderPDFViewController",
                      @"LL_HDCommonToolsVC",@"LL_WebServerViewController",@"LL_PingViewController",
                      nil];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self initData];
    
    self.navigationItem.title = @"我的";
    self.tabBarItem.title = @"我的";
    //    [self.navigationController.tabBarItem setBadgeValue:@"3"];
    __weak __typeof(self) weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        NSUInteger delaySeconds = 1;
        dispatch_time_t when = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delaySeconds * NSEC_PER_SEC));
        dispatch_after(when, dispatch_get_main_queue(), ^{
            [weakSelf.tableView.mj_header endRefreshing];
        });
    }];
    self.tableView.tableFooterView = [UIView new];

}

- (void)refresh {
    [self.tableView.mj_header beginRefreshing];
    NSUInteger delaySeconds = 1;
    dispatch_time_t when = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delaySeconds * NSEC_PER_SEC));
    dispatch_after(when, dispatch_get_main_queue(), ^{
        [self.tableView.mj_header endRefreshing];
    });
}
#pragma mark - Methods

- (void)configureCell:(UITableViewCell *)cell forIndexPath:(NSIndexPath *)indexPath {
    [[cell textLabel] setText:[NSString stringWithFormat:@"🔥🔥👉 🌜 %@ 🌛 👈", [self.nameArray objectAtIndex:indexPath.row]]];
}

#pragma mark - Table view

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [self configureCell:cell forIndexPath:indexPath];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.vcArray count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    [self cyl_showBadgeValue:[NSString stringWithFormat:@"%@", @(indexPath.row)] animationType:CYLBadgeAnimationTypeNone];
    
    NSString *string = [self.vcArray objectAtIndex:indexPath.row];
    Class myClass = NSClassFromString(string);
    id newClass = [[myClass alloc] init];
//    [newClass cyl_setNavigationBarHidden:YES];
    [self.navigationController pushViewController:newClass animated:YES];

    
}

- (void)testPush {
    UIViewController *viewController = [[LL_DetailsViewController alloc] init];
    viewController.view.backgroundColor = [UIColor redColor];
    [self.navigationController pushViewController:viewController animated:YES];
}


@end
