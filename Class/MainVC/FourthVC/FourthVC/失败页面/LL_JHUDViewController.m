//
//  LL_JHUDViewController.m
//  XJW
//
//  Created by John on 2020/1/23.
//  Copyright © 2020 Liam. All rights reserved.
//

#import "LL_JHUDViewController.h"
#import "LL_JHUDDetailViewController.h"

@interface LL_JHUDViewController () <UITableViewDelegate,UITableViewDataSource>

@property (strong , nonatomic) UITableView *tableView;

@property (strong , nonatomic) NSArray  *datas;

@end

@implementation LL_JHUDViewController

-(UITableView *)tableView
{
    if(!_tableView){
        _tableView = [[UITableView alloc] initWithFrame:self.view.frame];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
    }
    return _tableView;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    
    [self.view addSubview:self.tableView];
    self.datas = @[@"circleAnimation",
                   @"circleJoinAnimation",
                   @"dotAnimation",
                   @"customAnimation",
                   @"gifAnimations",
                   @"failure",
                   @"failure2",
                   @"classMethod",
                   ];
    
    [self.tableView reloadData];
}



#pragma mark  --  <UITableViewDelegate,UITableViewDataSource>

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _datas.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * cellID = @"JHUD";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = _datas[indexPath.row];

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

    LL_JHUDDetailViewController * detailVC = [LL_JHUDDetailViewController new];
    detailVC.selName = _datas[indexPath.row];
    [self.navigationController pushViewController:detailVC animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
