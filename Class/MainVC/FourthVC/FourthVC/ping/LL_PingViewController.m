//
//  LL_PingViewController.m
//  XJW
//
//  Created by John on 2019/7/31.
//  Copyright © 2019 Liam. All rights reserved.
//

#import "LL_PingViewController.h"
//  多ping
#import "NENPingManager.h"
//  单ping
//#import "FFSimplePingHelper.h"
#import "PPSPingServices.h"


@interface LL_PingViewController () <UITextFieldDelegate,UITextViewDelegate>

@property (strong , nonatomic) NENPingManager* pingManager;

@property (strong , nonatomic) UIButton *singlePingBtn;
@property (strong , nonatomic) UIButton *manyPingBtn;

//#warning - 一定要定义全局的变量
//@property (nonatomic,strong) FFSimplePingHelper *simplePingHelper;

@property (nonatomic, strong) PPSPingServices *service;
@property (nonatomic, strong) UITextField *pingTextView;
@property (nonatomic, strong) UITextView *pingResultTextView;
@property (nonatomic, strong) NSMutableString *resultString;


@end

@implementation LL_PingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self showMainView];
}


-(void)showMainView
{
    self.pingTextView =[[UITextField alloc] initWithFrame:CGRectMake(0, 64, kScreenWidth, 44)];
    self.pingTextView.delegate = self;
    self.pingTextView.placeholder = @"请输入网址";
    self.pingTextView.textColor = [UIColor grayColor];
    
    UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 50, 44)];
    lable.text = @"🔥🔥";
    self.pingTextView.leftView = lable;
    self.pingTextView.leftViewMode = UITextFieldViewModeAlways;
    self.pingTextView.layer.cornerRadius = 6;
    self.pingTextView.layer.borderWidth = 1;
    self.pingTextView.layer.borderColor = [UIColor grayColor].CGColor;
    self.pingTextView.keyboardType = UIKeyboardTypeURL;
//    不自动大写
    self.pingTextView.autocapitalizationType = UITextAutocapitalizationTypeNone;
//    不自动更正
    self.pingTextView.autocorrectionType = UITextAutocorrectionTypeNo;
//    键盘外观
    self.pingTextView.keyboardAppearance = UIKeyboardAppearanceAlert;

    [self.view addSubview:self.pingTextView];
    
    
    
    self.pingResultTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 64 +44, kScreenWidth, kScreenHeight - 64 - 50 -44)];
    self.pingResultTextView.delegate = self;
    [self.pingResultTextView.layoutManager setAllowsNonContiguousLayout:NO];
    [self.pingResultTextView setEditable:NO];
    [self.view addSubview:self.pingResultTextView];
    
    
    self.singlePingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.singlePingBtn.frame = CGRectMake(0, kScreenHeight-50, kScreenWidth/2, 50);
    [self.singlePingBtn setTitle:@"单ping开始" forState:UIControlStateNormal];
    [self.singlePingBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.singlePingBtn.backgroundColor = [UIColor orangeColor];
    [self.view addSubview: self.singlePingBtn];
    [self.singlePingBtn addTarget:self action:@selector(singlePingBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.manyPingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.manyPingBtn.frame = CGRectMake(kScreenWidth/2, kScreenHeight-50, kScreenWidth/2, 50);
    [self.manyPingBtn setTitle:@"多ping开始" forState:UIControlStateNormal];
    [self.manyPingBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.manyPingBtn.backgroundColor = [UIColor orangeColor];
    [self.view addSubview: self.manyPingBtn];
    [self.manyPingBtn addTarget:self action:@selector(manyPingBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)singlePingBtnPressed
{
    
//    self.simplePingHelper = [[FFSimplePingHelper alloc] initWithHostName:@"www.apple.com"];
//    [self.simplePingHelper startPing];
    
    self.resultString = [[NSMutableString alloc] initWithString:@" ping结果:\n\n"];
    
    if (self.pingTextView.text.length > 0 ) {
        self.service = [PPSPingServices serviceWithAddress:self.pingTextView.text];
    }else{
        self.service = [PPSPingServices serviceWithAddress:@"www.qq.com"];
    }
    __weakSelf__
    [self.service startWithCallbackHandler:^(PPSPingSummary *pingItem, NSArray *pingItems) {
        NSLog(@"%@",pingItem);
        [weakSelf.resultString appendFormat:@"   %@\n",pingItem];
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.pingResultTextView.text = weakSelf.resultString;
            [weakSelf.pingResultTextView scrollRangeToVisible:NSMakeRange(weakSelf.pingResultTextView.text.length,1)];

        });
    }];
}

-(void)manyPingBtnPressed
{
    [self manyPing];
}




-(void)manyPing
{
    NSArray *hostNameArray = @[
                               @"www.bilibili.com",
                               @"www.baidu.com",
                               @"www.youku.com",
                               @"www.hao123.com"
                               ];
    self.pingManager = [[NENPingManager alloc] init];
    [self.pingManager getFatestAddress:hostNameArray completionHandler:^(NSString *hostName, NSArray *sortedAddress) {
        NSLog(@"fastest IP: %@",hostName);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
