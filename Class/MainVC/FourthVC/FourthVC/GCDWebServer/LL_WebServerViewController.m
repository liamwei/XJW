//
//  LL_WebServerViewController.m
//  XJW
//
//  Created by John on 2019/7/30.
//  Copyright © 2019 Liam. All rights reserved.
//

#import "LL_WebServerViewController.h"
#import "GCDWebUploader.h"
#import "SJXCSMIPHelper.h"
#import "UILabel+Copy.h"

@interface LL_WebServerViewController ()<GCDWebUploaderDelegate,GCDWebServerDelegate, UITableViewDelegate, UITableViewDataSource>
{
    GCDWebUploader * _webServer;
}

/* 显示ip地址 */
@property (nonatomic, weak) UILabel *showIpLabel;
@property (nonatomic, weak) UILabel *showOtherIpLabel;

/* fileTableView */
@property (nonatomic, weak) UITableView *fileTableView;
/* fileArray */
@property (nonatomic, strong) NSMutableArray *fileArray;

@end

@implementation LL_WebServerViewController


-(void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // 文件存储位置
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSLog(@"文件存储位置 : %@", documentsPath);
    
    // 创建webServer，设置根目录
    _webServer = [[GCDWebUploader alloc] initWithUploadDirectory:documentsPath];
    // 设置代理
    _webServer.delegate = self;
    _webServer.allowHiddenItems = YES;
    
    // 限制文件上传类型
//    _webServer.allowedFileExtensions = @[@"doc", @"docx", @"xls", @"xlsx", @"txt", @"pdf"];
    // 设置网页标题
    _webServer.title = @"Liam 的demo";
    // 设置展示在网页上的文字(开场白)
    _webServer.prologue = @"欢迎使用 Liam 的WIFI管理平台";
    // 设置展示在网页上的文字(收场白)
    _webServer.epilogue = @"Liam 制作";
    
    if ([_webServer start]) {
        self.showIpLabel.hidden = NO;
        self.showOtherIpLabel.text = [NSString stringWithFormat:@"http://iPhone.local/"];
        self.showIpLabel.text = [NSString stringWithFormat:@"请在网页输入这个地址  http://%@:%zd/", [SJXCSMIPHelper deviceIPAdress], _webServer.port];
        
    } else {
        self.showIpLabel.text = NSLocalizedString(@"GCDWebServer not running!", nil);
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [_webServer stop];
    _webServer = nil;
}

#pragma mark - <UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fileArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.textLabel.text = self.fileArray[indexPath.row];
    return cell;
}

#pragma mark - <GCDWebUploaderDelegate>
- (void)webUploader:(GCDWebUploader*)uploader didUploadFileAtPath:(NSString*)path {
    NSLog(@"[UPLOAD] %@", path);
    
    self.showIpLabel.hidden = YES;
    
    NSString *string = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    self.fileArray = [NSMutableArray arrayWithArray:[fileManager contentsOfDirectoryAtPath:string error:nil]];
    
    [self.fileTableView reloadData];
}

- (void)webUploader:(GCDWebUploader*)uploader didMoveItemFromPath:(NSString*)fromPath toPath:(NSString*)toPath {
    NSLog(@"[MOVE] %@ -> %@", fromPath, toPath);
}

- (void)webUploader:(GCDWebUploader*)uploader didDeleteItemAtPath:(NSString*)path {
    NSLog(@"[DELETE] %@", path);
    
    NSString *string = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    self.fileArray = [NSMutableArray arrayWithArray:[fileManager contentsOfDirectoryAtPath:string error:nil]];
    
    [self.fileTableView reloadData];
}

- (void)webUploader:(GCDWebUploader*)uploader didCreateDirectoryAtPath:(NSString*)path {
    NSLog(@"[CREATE] %@", path);
}

#pragma mark - <懒加载>
- (UILabel *)showIpLabel {
    if (!_showIpLabel) {
        UILabel *lb = [[UILabel alloc] init];
        lb.frame = CGRectMake(0, kScreenHeight/3, kScreenWidth, 50);
//        lb.bounds = CGRectMake(0, kScreenHeight/2, kScreenWidth, 50);
//        lb.center = CGPointMake(kScreenHeight * 0.5, kScreenWidth * 0.5);
        lb.textColor = [UIColor darkGrayColor];
        lb.textAlignment = NSTextAlignmentCenter;
        lb.font = [UIFont systemFontOfSize:13.0];
        lb.numberOfLines = 0;
        lb.copyingEnabled = YES;

        [self.view addSubview:lb];
        _showIpLabel = lb;
    }
    return _showIpLabel;
}

-(UILabel *)showOtherIpLabel
{
    if (!_showOtherIpLabel) {
        UILabel *lb = [[UILabel alloc] init];
        lb.frame = CGRectMake(0, kScreenHeight/3 - 50, kScreenWidth, 50);
        lb.textColor = [UIColor darkGrayColor];
        lb.textAlignment = NSTextAlignmentCenter;
        lb.font = [UIFont systemFontOfSize:13.0];
        lb.numberOfLines = 0;
        lb.copyingEnabled = YES;

        [self.view addSubview:lb];
        _showOtherIpLabel = lb;
    }
    return _showOtherIpLabel;
}



- (UITableView *)fileTableView {
    if (!_fileTableView) {
        UITableView *tv = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStylePlain];
        
        // 设置代理
        tv.delegate = self;
        // 设置数据源
        tv.dataSource = self;
        // 清除表格底部多余的cell
        tv.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        
        [self.view addSubview:tv];
        _fileTableView = tv;
    }
    return _fileTableView;
}
- (NSMutableArray<NSString *> *)fileArray {
    if (!_fileArray) {
        _fileArray = [NSMutableArray array];
    }
    return _fileArray;
}


- (void)webServerDidCompleteBonjourRegistration:(GCDWebServer*)server
{
    NSLog(@"%@---------%@----%@",server.bonjourName, server.bonjourServerURL,server.serverURL);
    self.showOtherIpLabel.text = [NSString stringWithFormat:@"%@",server.bonjourServerURL];
    self.showIpLabel.text = [NSString stringWithFormat:@"请在网页输入这个地址  %@:%zd", server.serverURL, server.port];

}

@end
