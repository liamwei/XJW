//
//  LL_ReaderPDFViewController.m
//  XJW
//
//  Created by John on 2020/2/20.
//  Copyright © 2020 Liam. All rights reserved.
//

#import "LL_ReaderPDFViewController.h"
#import "ReaderViewController.h"
#import "DocTableViewController.h"

@interface LL_ReaderPDFViewController ()<UITableViewDelegate,UITableViewDataSource,ReaderViewControllerDelegate>

@property (nonatomic , strong) NSArray * dataArray;
@property (nonatomic , strong) NSArray * dataArray1;

@end

@implementation LL_ReaderPDFViewController
{
    ReaderViewController *readerViewController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.dataArray = [NSArray arrayWithObjects:@"阅读 PDF",@"文档管理",nil];
    self.dataArray1 = [NSArray arrayWithObjects:@"Reade PDF",@"Document Management",nil];

    
    [self showMainView];
    
}

-(void)showMainView
{
    UIView *back = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 160)];
    [self.view addSubview:back];
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 62 + 32, kScreenWidth, 33)];
    label.text = @"Reader PDF";
    label.font = [UIFont systemFontOfSize:27];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor lightGrayColor];
    [back addSubview:label];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 160, kScreenWidth, kScreenHeight - 160 - 64)];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.tableFooterView = [UIView new];
    [self.view addSubview:tableView];
}






- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = self.dataArray[indexPath.row];
    cell.detailTextLabel.text = self.dataArray1[indexPath.row];

    return cell;
}





- (void)pushPDFReaderViewController {
    NSString *phrase = nil; // Document password (for unlocking most encrypted PDF files)
    NSArray *pdfs = [[NSBundle mainBundle] pathsForResourcesOfType:@"pdf" inDirectory:nil];
    NSString *filePath = [pdfs lastObject]; assert(filePath != nil); // Path to last PDF file
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:phrase];
    
    if (document != nil) {
        readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        readerViewController.delegate = self; // Set the ReaderViewController delegate to self
        [self presentViewController:readerViewController animated:YES completion:^{
        }];
    }
}

#pragma mark - ReaderViewControllerDelegate methods
- (void)dismissReaderViewController:(ReaderViewController *)viewController {
#if (DEMO_VIEW_CONTROLLER_PUSH == TRUE)
    [self.navigationController popViewControllerAnimated:YES];
#else
    [self dismissViewControllerAnimated:YES completion:NULL];
#endif
}

#pragma mark - Table view data source
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        [self pushPDFReaderViewController];
    }else if (indexPath.row == 1){
        DocTableViewController *vc = [[DocTableViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}








@end
