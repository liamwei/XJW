//
//  AppDelegate.m
//  XJW
//
//  Created by John on 2019/7/27.
//  Copyright © 2019 Liam. All rights reserved.
//

#import "AppDelegate.h"
#import "AppDelegate+JPush.h"
#import "LL_CYLPlusButtonSubclass.h"
#import "LL_MainRootViewController.H"
#import "ZYNetworkAccessibity.h"

#import <MSLaunchView.h>


@interface AppDelegate ()<MSLaunchViewDeleagte>
{
    MSLaunchView *_launchView;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    LL_MainRootViewController *rootViewController = [[LL_MainRootViewController alloc] init];
    [self.window setRootViewController:rootViewController];
    [self.window makeKeyAndVisible];
    
//    启动页
    [self showFirstLaunch:launchOptions];
    
    [self setUpNavigationBarAppearance];
    
    [self initSomeInfo];
   
    [self newWorkAuthority];

    return YES;
}





-(void)newWorkAuthority
{
//    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    //Set SVProgressHUD
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];

    
    [ZYNetworkAccessibity setAlertEnable:YES];
    [ZYNetworkAccessibity setStateDidUpdateNotifier:^(ZYNetworkAccessibleState state) {
        NSLog(@"setStateDidUpdateNotifier > %zd", state);
    }];
    [ZYNetworkAccessibity start];
}



#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmethod-signatures"
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    //设置强制旋转屏幕
    if (self.cyl_isForceLandscape) {
        //只支持横屏
        return UIInterfaceOrientationMaskLandscape;
    } else {
        //只支持竖屏
        return UIInterfaceOrientationMaskPortrait;
    }
}
#pragma clang diagnostic pop

/**
 *  设置navigationBar样式
 */
- (void)setUpNavigationBarAppearance {
    UINavigationBar *navigationBarAppearance = [UINavigationBar appearance];
    UIColor *backgroundColor = [UIColor cyl_systemBackgroundColor];
    NSDictionary *textAttributes = nil;
    UIColor *labelColor =   [UIColor cyl_labelColor];
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        textAttributes = @{
                           NSFontAttributeName : [UIFont boldSystemFontOfSize:18],
                           NSForegroundColorAttributeName : labelColor,
                           };
    } else {
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
        textAttributes = @{
                           UITextAttributeFont : [UIFont boldSystemFontOfSize:18],
                           UITextAttributeTextColor : labelColor,
                           UITextAttributeTextShadowColor : [UIColor clearColor],
                           UITextAttributeTextShadowOffset : [NSValue valueWithUIOffset:UIOffsetZero],
                           };
#endif
    }
    [navigationBarAppearance setBarTintColor:backgroundColor];
    [navigationBarAppearance setTitleTextAttributes:textAttributes];
}




- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}









/**
 iOS 9.0 以下 程序运行过程中调用
 */
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    
    NSLog(@"URL scheme:%@", [url scheme]);
    NSLog(@"URL host:%@", [url host]);
    return YES;
}

/**
 iOS 9.0 之后 程序运行过程中调用
 */
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options{
    
    NSLog(@"URL scheme:%@", [url scheme]);
    //参数
    NSLog(@"URL host:%@", [url host]);
    
    NSString * message;
    if ([[url host] isEqualToString:@"success"]) {
        message = @"分享成功";
    }else{
        message = @"分享失败";
    }
    
    UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"提示" message:[url host] delegate:self cancelButtonTitle:nil otherButtonTitles:message, nil];
    [alertView show];
    return YES;
}

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//}

#pragma -mark 启动页处理

-(void)showFirstLaunch:(NSDictionary *)launchOptions
{
//     if (kisFirstLaunch) {//或者使用[MSLaunchView isFirstLaunch]判断是否是第一次进入
            NSArray *imageNameArray = @[@"Untitled-4.gif",@"Untitled-6.gif",@"Untitled-7.gif"];
            CGRect rt = CGRectMake(kScreenWidth*0.3, kScreenHeight*0.8, kScreenWidth*0.4, kScreenHeight*0.08);
            
    #pragma mark ---->>带立即体验按钮，项目是SB创建的 (此方法初始化，则guideBtnCustom：方法失效)
    //        MSLaunchView *launchView = [MSLaunchView launchWithImages:imageNameArray sbName:@"" guideFrame:rt gImage:[UIImage imageNamed:@""] isScrollOut:YES];
    #pragma mark ---->>不带立即体验按钮 项目是SB创建的
    //        MSLaunchView *launchView = [MSLaunchView launchWithImages:imageNameArray sbName:@"" isScrollOut:NO];
    #pragma mark ---->>不带立即按钮 代码创建项目
    //        MSLaunchView *launchView = [MSLaunchView launchWithImages:imageNameArray isScrollOut:YES];
    #pragma mark ---->>带立即按钮 代码创建项目
            MSLaunchView *launchView = [MSLaunchView launchWithImages:imageNameArray guideFrame:rt gImage:[UIImage imageNamed:@""] isScrollOut:NO];

            
    #pragma mark ---->>关于Video 没有立即进入按钮
    //        NSString *path  = [[NSBundle mainBundle]  pathForResource:@"测试" ofType:@"mp4"];
    //        NSURL *url = [NSURL fileURLWithPath:path];
    //        MSLaunchView *launchView = [MSLaunchView launchWithVideo:CGRectMake(0, 0, MSScreenW, MSScreenH) videoURL:url];
    //        launchView.videoGravity = AVLayerVideoGravityResize;
    //        launchView.isPalyEndOut = YES;//
            
            launchView.guideTitle = @"进入当前界面";
            launchView.guideTitleColor = [UIColor blackColor];
            launchView.guideTitleFont = [UIFont systemFontOfSize:17];
            
    #pragma mark ---->>跳过按钮自定义属性
            launchView.skipTitle = @"跳过";
            launchView.skipTitleColor = [UIColor whiteColor];
            launchView.skipTitleFont = [UIFont systemFontOfSize:15];
            launchView.skipBackgroundColor = [UIColor redColor];
            launchView.skipBackgroundImage = [UIImage imageNamed:@""];
            
            
    #pragma mark ---->>PageControl自定义属性

    //        launchView.showPageControl = NO;
            //pageControl的间距大小
    //        launchView.pageControlStyle = MSPageControlStyleNumber;
         
    //        launchView.pageControlBottomOffset += 25;
            launchView.pageDotColor = [UIColor redColor];
            launchView.currentPageDotColor = [UIColor yellowColor];
            launchView.textFont = [UIFont systemFontOfSize:9 weight:UIFontWeightBold];
            launchView.textColor = [UIColor blackColor];
            launchView.dotsIsSquare = NO;
            launchView.spacingBetweenDots = 15;
          
            launchView.dotBorderWidth = 2;
            launchView.dotBorderColor = [UIColor blueColor];
            launchView.currentDotBorderWidth = 2;
            launchView.currentDotBorderColor = [UIColor redColor];
            launchView.delegate = self;
            //设置了pageDotImage和currentPageDotImage图片 pageControlDotSize和currentWidthMultiple将失效
            launchView.pageControlDotSize = CGSizeMake(20, 20);
            launchView.currentWidthMultiple =  3;
            
            launchView.lastDotsIsHidden = YES;//最后一个页面时是否隐藏PageControl 默认为NO
            launchView.pageDotImage = [UIImage imageNamed:@"Ktv_ic_share_qq"];
            launchView.currentPageDotImage = [UIImage imageNamed:@"Ktv_ic_share_weixin"];
            launchView.loadFinishBlock = ^(MSLaunchView * _Nonnull launchView) {
                NSLog(@"广告加载完成了");
            };
            
    #pragma mark ---->>自定义立即体验按钮
            [launchView guideBtnCustom:^UIButton * _Nonnull{
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                btn.frame = CGRectMake(60, 60, 130, 60);
                [btn setBackgroundColor:[UIColor redColor]];
                [btn setTitle:@"立即体验" forState:UIControlStateNormal];
                [btn addTarget:self action:@selector(hidde) forControlEvents:UIControlEventTouchUpInside];
                return btn;
            }];
            
    #pragma mark ---->>自定义跳过按钮

    //        [launchView skipBtnCustom:^UIButton * _Nonnull{
    //            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //            btn.frame = CGRectMake(60, 200, 120, 120);
    //            [btn setBackgroundColor:[UIColor blueColor]];
    //            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //            [btn setTitle:@"跳过按钮" forState:UIControlStateNormal];
    //            [btn addTarget:self action:@selector(hidde) forControlEvents:UIControlEventTouchUpInside];
    //            return btn;
    //        }];

            _launchView = launchView;
//     }
}

-(void)hidde{
    [_launchView hideGuidView];
}

-(void)launchViewLoadFinish:(MSLaunchView *)launchView{
    NSLog(@"代理方法进入======广告加载完成了");
}




@end
